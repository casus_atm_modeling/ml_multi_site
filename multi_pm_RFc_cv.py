import pylab
#pandas
import pandas as pd
print('pandas: %s' % pd.__version__)
#matplotlib
import matplotlib.pyplot as plt
#datetime
import datetime as dt
#import numpy
import numpy as np
#sklearn
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
#gaussian_kde
from scipy.stats import gaussian_kde
#anchored text
from matplotlib.offsetbox import AnchoredText 
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import RFE
from sklearn.model_selection import RepeatedKFold
import time
import glob
import pickle
import os

#open functions
exec(open(r"./ML_Functions.py").read())

gmtoff = pd.read_csv('./gmtoff.csv')

with open('./data/WRF_pkl/multi_sites_full_pm_1720.pkl', 'rb') as fp:
    dict_of_dt = pickle.load(fp)

#create the results dir if it doesn't exist
outputdir='./output/'
os.makedirs(outputdir, exist_ok=True)

for s in list(dict_of_dt.keys()):
    if os.path.isfile(outputdir+'data_pm_RFc_'+s+'_winter.csv'): continue
    aqsid = s
    print(aqsid)
    
    dfPm = dict_of_dt[s]
    #dfPm = dfPm[(dfPm.index.year<2020)]
    dfPm = dfPm[(dfPm.index.month > 10) | (dfPm.index.month < 3)]
    #dfPm = dfPm.drop('PM2.5_pred',1).dropna()
    if(len(dfPm)==0): continue
    
    try: 
        f = open("./data/WRF_pkl/data_wrf2020_"+s+".pkl",'rb')
        try:
            obs_ap_tmp = pd.read_csv(r"http://lar.wsu.edu/R_apps/2020ap5/data/byAQSID/"+s+".apan")
        except:
            print('2020 data is not available')
        obs_ap=obs_ap_tmp
        
        #obs_ap=pd.read_csv(r"http://lar.wsu.edu/R_apps/2019ap5/data/byAQSID/530050003.apan")
        obs_ap['DateTime'] = pd.to_datetime(obs_ap['DateTime'])
        obs_ap.index = obs_ap['DateTime']
        delta = np.timedelta64(8,'h')
        obs_ap.index = obs_ap.index - delta
        
        obs_ap['PM2.5_obs']=obs_ap['PM2.5an'].copy()
        obs_ap['PM2.5_mod']=obs_ap['PM2.5ap'].copy()
        #obs_ap.iloc[-24:,1]=np.nan
        #obs_ap=obs_ap.iloc[0:-24,].copy()
        #
        r = pd.date_range(start=obs_ap.index.min(), end=obs_ap.index.max(), freq='1H')
        obs_ap = obs_ap.reindex(r)
        
        obs_ap['PMavg24hr'] = obs_ap['PM2.5_obs'].rolling(24, min_periods=18).mean()
        obs_ap['PMavg24hr_ap'] = obs_ap['PM2.5_mod'].rolling(24, min_periods=18).mean()
        #fill na with ML forecasting
        #obs_ap['PMavg24hr']=obs_ap['PMavg24hr'].fillna(dict_his_tmp['mean'][dict_his_tmp['mean'].index.date<d]['PM2.5_pred'])
        obs_ap['AQI_class'] = pd.cut(round(obs_ap['PMavg24hr'].fillna(-1)),
            [0, 12, 35.4, 55.4, 150.4, 250.4, np.inf],
            labels=[1, 2, 3, 4, 5, 6])
        obs_ap['AQI_class_ap'] = pd.cut(round(obs_ap['PMavg24hr_ap'].fillna(-1)),
            [0, 12, 35.4, 55.4, 150.4, 250.4, np.inf],
            labels=[1, 2, 3, 4, 5, 6])
        obs_ap['Past_hr_PM'] = obs_ap['PM2.5_obs'].shift(24)
        obs_ap['Past_24hr_PM'] = obs_ap['PMavg24hr'].shift(24)
        obs_ap= obs_ap[['PM2.5_obs','PM2.5_mod','Past_hr_PM','Past_24hr_PM','AQI_class','PMavg24hr','AQI_class_ap','PMavg24hr_ap']]
        if len(obs_ap.dropna())==0: continue
        
        dict_his2 = pickle.load(f)
        df_tmp = dfPm.combine_first(dict_his2['WRFRT'])
        
        df_tmp = df_tmp.combine_first(obs_ap)
        df_tmp.update(obs_ap)
        
        #replace AIRNOW data with AQS data
        pm_88101 = glob.glob('./data/AQS_data_US/88101/'+s+'_*_88101.csv')
        pm_88502 = glob.glob('./data/AQS_data_US/88502/'+s+'_*_88502.csv')
        aqs_pm = pm_88101+pm_88502
        if len(aqs_pm) > 0:
            for pm_file in aqs_pm:
                aqs = pd.read_csv(aqs_pm[pm_file])
                aqs = aqs[aqs['sample_duration']=='1 HOUR']
                if(len(aqs)==0): continue
                else: break
            aqs.index = pd.to_datetime(aqs.Datetime)
            aqs = aqs.drop_duplicates(subset='Datetime', keep="first") #There are duplicates at some sites. Based on the pre-gernerated data file, the first value should be used
            #change time zone to local time
            delta = np.timedelta64(abs(gmtoff.loc[gmtoff['AQSID']==s,'GMToff'].values[0]),'h')
            obs_ap.index = obs_ap.index - delta
            aqs.columns = ['PM2.5_obs']
            df_tmp = pd.concat([df_tmp,aqs],axis=1).drop(columns=['Datetime', 'PM2.5_obs', 'units_of_measure', 'sample_duration'])
            df_tmp = df.rename(columns={"sample_measurement": "PM2.5_obs"})
        else:
            print('NO AQS data')
            
        df_tmp['PMavg24hr'] = df_tmp['PM2.5_obs'].rolling(24, min_periods=18).mean()
        #fill na with ML forecasting
        #obs_ap['PMavg24hr']=obs_ap['PMavg24hr'].fillna(dict_his_tmp['mean'][dict_his_tmp['mean'].index.date<d]['PM2.5_pred'])
        df_tmp['AQI_class'] = pd.cut(round(df_tmp['PMavg24hr'].fillna(-1)),
            [0, 12, 35.4, 55.4, 150.4, 250.4, np.inf],
            labels=[1, 2, 3, 4, 5, 6])
        df_tmp['Past_24hr_PM'] = df_tmp['PMavg24hr'].shift(24)
        
        df_tmp['Weekday']=df_tmp.index.dayofweek
        df_tmp['Month'] = df_tmp.index.month
        df_tmp['Hour'] = df_tmp.index.hour
        dfPm = df_tmp[['PM2.5_obs','PM2.5_mod','Past_hr_PM','PMavg24hr', 'PMavg24hr_ap', 'PBL_m', 'Surface_pres_Pa', 'Temp_K', 'U_m_s', 'V_m_s',
                                'RH_pct', 'Past_24hr_PM','Month','Hour','Weekday','AQI_class']].copy()
        dfPm = dfPm[(dfPm.index.month > 10) | (dfPm.index.month < 3)]
    except:     
        print('2020 data is not available at '+s)
    
    dfPm = dfPm.dropna()
    X_org = dfPm.drop(['PM2.5_obs', 'PM2.5_mod','PMavg24hr', 'PMavg24hr_ap','Past_hr_PM'], 1).copy()
    X_dat = pd.DataFrame(preprocess('MAS', X_org))
    X_dat.columns = X_org.keys()
    X_dat.index = X_org.index
    #training RF and MLR model
    X1 = np.array(X_dat.drop(['AQI_class'], 1).copy())
    # separate "label" to y 
    y1 = np.array(dfPm['AQI_class'])
    #X2 = np.array(X_dat.copy())
    # separate "label" to y 
    y2 = np.array(dfPm['PMavg24hr'])
    
    all_date = np.array(sorted(set(dfPm.index.date)))
    rkf = RepeatedKFold(n_splits=10, n_repeats=10, random_state=12883823)
    a = 0
    dict_of_2019=dict()
    dict_of_max2019=dict()
    for train_index, test_index in rkf.split(all_date):
        train_datetime_index = pd.to_datetime(dfPm.index.date).isin(all_date[train_index])
        test_datetime_index = pd.to_datetime(dfPm.index.date).isin(all_date[test_index])
        X_train1, X_test1 = X1[train_datetime_index], X1[test_datetime_index]
        y_train1, y_test1 = y1[train_datetime_index], y1[test_datetime_index]
        
        # feature extraction
        model_RF = RandomForestClassifier(n_estimators=100, 
                    bootstrap = True, max_depth=7,
                    max_features = 'sqrt',
                    #class_weight = dict({1:150, 2:10, 3:1})
                    random_state=137, class_weight = 'balanced_subsample')
        model_RF = model_RF.fit(X_train1, y_train1)
        pred_AQI = model_RF.predict(X_test1)
        #MLR   
        # create new np array without label
        X_dat2 = X_dat.copy()
        X_dat2.loc[test_datetime_index,'AQI_class'] = pred_AQI/max(dfPm['AQI_class'])
        X2 = np.array(X_dat2.copy())
        X_train2, X_test2 = X2[train_datetime_index], X2[test_datetime_index]
        y_train2, y_test2 = y2[train_datetime_index], y2[test_datetime_index]
        
        model_LR = LinearRegression()
        rfe = RFE(model_LR, 5)
        RFEfit = rfe.fit(X_train2, y_train2)
        PM25_pred = RFEfit.predict(X_test2)
        
        df2019 = dfPm[test_datetime_index].copy() 
        df2019 = df2019.assign(PM25_pred=PM25_pred)
        df2019.index = df2019.index
        
        dict_name = str(a)
        dict_of_2019[dict_name] = df2019
        
        temp = df2019.copy()
        PM25_2020 = temp[['PMavg24hr', 'PMavg24hr_ap', 'PM25_pred']].copy()
        r = pd.date_range(start=PM25_2020.index.min(), end=PM25_2020.index.max(), freq='1H')
        PM25_2020 = PM25_2020.reindex(r)
        PM25_2020['PMavg24hr_org'] = PM25_2020['PMavg24hr'].shift(-23)
        PM25_2020['PMavg24hr_RF'] = PM25_2020['PM25_pred'].shift(-23)
        PM25_2020['PMavg24hr_ap'] = PM25_2020['PMavg24hr_ap'].shift(-23)
        
        df2020dailyPM2524hr = PM25_2020[(PM25_2020.index.hour == 0)]#.dropna(how='all')
        
        df2020dailyPM2524hr['AQI_day'] = pd.cut(round(df2020dailyPM2524hr['PMavg24hr_org'],1),
                                [-np.inf, 12, 35.4, 55.4, 150.4, 250.4, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
        df2020dailyPM2524hr['AQI_pred_day'] = pd.cut(round(df2020dailyPM2524hr['PMavg24hr_RF'],1),
                                [-np.inf, 12, 35.4, 55.4, 150.4, 250.4, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
        df2020dailyPM2524hr['AQI_ap_day'] = pd.cut(round(df2020dailyPM2524hr['PMavg24hr_ap'],1),
                                [-np.inf, 12, 35.4, 55.4, 150.4, 250.4, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
        
        df2020dailyPM2524hr.index = df2020dailyPM2524hr.index+pd.DateOffset(hours=5)
        
        aqi_low=[0,51,101,151,201,301,401]
        aqi_high=[50,100,150,200,300,400,500]
        aqi_lowc=[0,12.1,35.5,55.5,150.5,250.5,350.5]
        aqi_highc=[12,35.4,55.4,150.4,250.4,350.4,500]
        df2020dailyPM2524hr['AQI']=np.nan
        for i in range(len(df2020dailyPM2524hr)):
            if(np.isnan(df2020dailyPM2524hr['PMavg24hr_RF'][i])): continue
            if(df2020dailyPM2524hr['PMavg24hr_RF'][i]<0): continue
            #df2020dailyPM2524hr['AQI'][i] = aqi.to_iaqi(aqi.POLLUTANT_PM2.5_8H, df2020dailyPM2524hr['PM2.5_pred.maxdaily8hravg'][i]/1000, algo=aqi.ALGO_EPA)
            aqi_class=df2020dailyPM2524hr['AQI_pred_day'][i]-1
            df2020dailyPM2524hr['AQI'][i]=(aqi_high[aqi_class]-aqi_low[aqi_class])/(aqi_highc[aqi_class]-aqi_lowc[aqi_class])*(round(df2020dailyPM2524hr['PMavg24hr_RF'][i],1)-aqi_lowc[aqi_class])+aqi_low[aqi_class]
            df2020dailyPM2524hr['AQI'][i]=round(df2020dailyPM2524hr['AQI'][i],1)
        
        dict_of_max2019[dict_name] = df2020dailyPM2524hr
        a=a+1
        
    for k in dict_of_max2019.keys():
        dict_of_max2019[k]['datetime']=dict_of_max2019[k].index
     
    big_df = pd.concat(dict_of_max2019)[['datetime','PMavg24hr_org','PMavg24hr_ap','PMavg24hr_RF']]
    big_df = big_df.groupby('datetime').mean().dropna(how='all')
    big_df['AQI_day'] = pd.cut(round(big_df['PMavg24hr_org']),
                            [-np.inf, 12, 35.4, 55.4, 150.4, 250.4, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
    big_df['AQI_pred_day'] = pd.cut(round(big_df['PMavg24hr_RF']),
                            [-np.inf, 12, 35.4, 55.4, 150.4, 250.4, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
    big_df['AQI_ap_day'] = pd.cut(round(big_df['PMavg24hr_ap']),
                            [-np.inf, 12, 35.4, 55.4, 150.4, 250.4, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
    big_df.to_csv(outputdir+'data_pm_RFc_'+s+'_winter.csv')
