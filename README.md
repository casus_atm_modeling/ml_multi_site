# Downloading air quality datasets from the EPA AQS API

### Private repositoy as it contains the account and key information.

### Please do not distribute this information to other

---

Here is how it works:

- request_AQS_data.sh : sbatch script that sets the species number as env variable and execute request_AQS_data.py script
  - species=88101 for 'PM2.5_FRM';'88502' for 'PM2.5_FRM'; and '44201' for 'O3'

- it will create the "download" directory in the upper level that will have the datasets you are looking for.

---

Here is the AQS API information: <https://aqs.epa.gov/aqsweb/documents/data_api.html>
