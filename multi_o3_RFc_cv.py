import pylab
#pandas
import pandas as pd
print('pandas: %s' % pd.__version__)
#matplotlib
import matplotlib.pyplot as plt
#datetime
import datetime as dt
#import numpy
import numpy as np
#sklearn
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
#gaussian_kde
from scipy.stats import gaussian_kde
#anchored text
from matplotlib.offsetbox import AnchoredText 
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import RFE
from sklearn.model_selection import RepeatedKFold
import time
import glob
import pickle
import os

#open functions
exec(open(r"./ML_Functions.py").read())

gmtoff = pd.read_csv('./gmtoff.csv')

with open('./data/WRF_pkl/multi_sites_full_o3_1720.pkl', 'rb') as fp:
    dict_of_dt = pickle.load(fp)

#create the results dir if it doesn't exist
outputdir='./output/'
os.makedirs(outputdir, exist_ok=True)

for s in list(dict_of_dt.keys()):
    #if os.path.isfile(outputdir+'data_o3_RFc_'+s+'.csv'): continue
    aqsid = s
    print(aqsid)
    
    dfPm = dict_of_dt[s]
    dfPm = dfPm[(dfPm.index.month >4) & (dfPm.index.month < 10)]
    #dfPm = dfPm.drop('O3_pred',1).dropna()
    if(len(dfPm)==0): continue
    
    try: 
        f = open("./data/WRF_pkl/data_wrf2020_"+s+".pkl",'rb')
        try:
            obs_ap_tmp = pd.read_csv(r"http://lar.wsu.edu/R_apps/2020ap5/data/byAQSID/"+s+".apan")
        except:
            print('2020 data is not available')
        obs_ap=obs_ap_tmp
        
        #obs_ap=pd.read_csv(r"http://lar.wsu.edu/R_apps/2019ap5/data/byAQSID/530050003.apan")
        obs_ap['DateTime'] = pd.to_datetime(obs_ap['DateTime'])
        obs_ap.index = obs_ap['DateTime']
        delta = np.timedelta64(8,'h')
        obs_ap.index = obs_ap.index - delta
        
        obs_ap['O3_obs']=obs_ap['OZONEan'].copy()
        obs_ap['O3_ap']=obs_ap['OZONEap'].copy()
        #obs_ap.iloc[-24:,1]=np.nan
        #obs_ap=obs_ap.iloc[0:-24,].copy()
        #
        r = pd.date_range(start=obs_ap.index.min(), end=obs_ap.index.max(), freq='1H')
        obs_ap = obs_ap.reindex(r)
        
        obs_ap['O3avg8hr'] = obs_ap['O3_obs'].rolling(8, min_periods=6).mean()
        obs_ap['O3avg8hr_ap'] = obs_ap['O3_ap'].rolling(8, min_periods=6).mean()
        #obs_ap['O3avg8hr'] = obs_ap['O3avg8hr'].shift(-7)
        #fill na with ML forecasting
        #obs_ap['O3avg8hr']=obs_ap['O3avg8hr'].fillna(dict_his_tmp['mean'][dict_his_tmp['mean'].index.date<d]['O3_pred'])
        obs_ap['AQI_class'] = pd.cut(round(obs_ap['O3avg8hr'].fillna(-1)),
            [0, 54, 70, 85, 105, 200, np.inf],
            labels=[1, 2, 3, 4, 5, 6])
        obs_ap['AQI_class_ap'] = pd.cut(round(obs_ap['O3avg8hr_ap'].fillna(-1)),
            [0, 54, 70, 85, 105, 200, np.inf],
            labels=[1, 2, 3, 4, 5, 6])
        obs_ap['Past_8hr_O3'] = obs_ap['O3avg8hr'].shift(24)
        obs_ap['Past_hr_O3'] = obs_ap['O3_obs'].shift(24)
        obs_ap = obs_ap[['Past_hr_O3','O3_obs','O3_ap','Past_8hr_O3','AQI_class','O3avg8hr','AQI_class_ap','O3avg8hr_ap']]
        if len(obs_ap.dropna())==0: continue
        
        dict_his2 = pickle.load(f)
        df_tmp = dfPm.combine_first(dict_his2['WRFRT'])
        
        df_tmp = df_tmp.combine_first(obs_ap)
        df_tmp.update(obs_ap)
        
        aqs_o3 = glob.glob('./data/AQS_data_US/44201/'+s+'_*_88101.csv')
        if len(aqs_o3) > 0:
            for o3_file in aqs_o3:
                aqs = pd.read_csv(aqs_o3[o3_file])
                aqs = aqs[aqs['sample_duration']=='1 HOUR']
                if(len(aqs)==0): continue
                else: break
            aqs.index = pd.to_datetime(aqs.Datetime)
            #change time zone to local time
            delta = np.timedelta64(abs(gmtoff.loc[gmtoff['AQSID']==s,'GMToff'].values[0]),'h')
            obs_ap.index = obs_ap.index - delta
            aqs.columns = ['O3_obs']
            df_tmp = pd.concat([df_tmp,aqs],axis=1).drop(columns=['Datetime', 'O3_obs', 'units_of_measure', 'sample_duration'])
            df_tmp = df.rename(columns={"sample_measurement": "O3_obs"})
        else:
            print('NO AQS data')
        
        df_tmp['O3avg8hr'] = df_tmp['O3_obs'].rolling(8, min_periods=6).mean()
        #obs_ap['O3avg8hr'] = obs_ap['O3avg8hr'].shift(-7)
        #fill na with ML forecasting
        #obs_ap['O3avg8hr']=obs_ap['O3avg8hr'].fillna(dict_his_tmp['mean'][dict_his_tmp['mean'].index.date<d]['O3_pred'])
        df_tmp['AQI_class'] = pd.cut(round(df_tmp['O3avg8hr'].fillna(-1)),
            [0, 54, 70, 85, 105, 200, np.inf],
            labels=[1, 2, 3, 4, 5, 6])
        df_tmp['Past_8hr_O3'] = df_tmp['O3avg8hr'].shift(24)
        
        df_tmp['Weekday']=df_tmp.index.dayofweek
        df_tmp['Month'] = df_tmp.index.month
        df_tmp['Hour'] = df_tmp.index.hour
        dfPm = df_tmp[['Past_hr_O3','O3_obs','O3_ap','O3avg8hr', 'O3avg8hr_ap', 'PBL_m', 'Surface_pres_Pa', 'Temp_K', 'U_m_s', 'V_m_s',
                            'RH_pct', 'Past_8hr_O3','Month','Hour','Weekday','AQI_class']].copy()
        dfPm = dfPm[(dfPm.index.month > 4 ) & (dfPm.index.month < 10)]
    except:     
        print('2020 data is not available at '+s)
    
    delta = np.timedelta64(7,'h')
    dfPm.index = dfPm.index - delta
    dfPm = dfPm.dropna()
    X_org = dfPm.drop(['O3_obs', 'O3_ap', 'O3avg8hr', 'O3avg8hr_ap', 'Past_hr_O3'], 1).copy()
    X_dat = pd.DataFrame(preprocess('MAS', X_org))
    X_dat.columns = X_org.keys()
    X_dat.index = X_org.index
    
    #training RF and MLR model
    X1 = np.array(X_dat.drop(['AQI_class'], 1).copy())
    # separate "label" to y 
    y1 = np.array(dfPm['AQI_class'])
    #X2 = np.array(X_dat.copy())
    # separate "label" to y 
    y2 = np.array(dfPm['O3avg8hr'])
    
    #test
    #X = np.array(sorted(set(subdf.index.date)))
    #rkf = RepeatedKFold(n_splits=10, n_repeats=1, random_state=12883823)
    #for train_index,test_index in rkf.split(X):
    #    tmp = subdf[pd.to_datetime(subdf.index.date).isin(X[test_index])]
    #    print(tmp['Weekday'].value_counts().sort_index())
        
    all_date = np.array(sorted(set(dfPm.index.date)))
    rkf = RepeatedKFold(n_splits=10, n_repeats=10, random_state=12883823)
    a = 0
    dict_of_2019=dict()
    dict_of_max2019=dict()
    RF_feature_table = pd.DataFrame(columns = dfPm.keys()[2:-1]) 
    MLR_feature_table = pd.DataFrame(columns = dfPm.keys()[2:])
    for train_index, test_index in rkf.split(all_date):
        train_datetime_index = pd.to_datetime(dfPm.index.date).isin(all_date[train_index])
        test_datetime_index = pd.to_datetime(dfPm.index.date).isin(all_date[test_index])
        X_train1, X_test1 = X1[train_datetime_index], X1[test_datetime_index]
        y_train1, y_test1 = y1[train_datetime_index], y1[test_datetime_index]
        
        # feature extraction
        model_RF = RandomForestClassifier(n_estimators=100, 
                    bootstrap = True, max_depth=7,
                    max_features = 'sqrt',
                    #class_weight = dict({1:150, 2:10, 3:1})
                    random_state=137, class_weight = 'balanced_subsample')
        model_RF = model_RF.fit(X_train1, y_train1)
        pred_AQI = model_RF.predict(X_test1)
        #MLR   
        # create new np array without label
        X_dat2 = X_dat.copy()
        X_dat2.loc[test_datetime_index,'AQI_class'] = pred_AQI/max(dfPm['AQI_class'])
        X2 = np.array(X_dat2.copy())
        X_train2, X_test2 = X2[train_datetime_index], X2[test_datetime_index]
        y_train2, y_test2 = y2[train_datetime_index], y2[test_datetime_index]
                         
         
        model_LR = LinearRegression()
        rfe = RFE(model_LR, 5)
        RFEfit = rfe.fit(X_train2, y_train2)
        O3_pred = RFEfit.predict(X_test2)
                           
        df2019 = dfPm[test_datetime_index].copy() 
        df2019 = df2019.assign(O3_pred=O3_pred)
        df2019.index = df2019.index + delta
        
        dict_name = str(a)
        dict_of_2019[dict_name] = df2019
        
        temp = df2019.copy()
        o32018 = temp[['O3_pred', 'O3avg8hr', 'O3avg8hr_ap']].copy()
        r = pd.date_range(start=o32018.index.min(), end=o32018.index.max(), freq='1H')
        o32018 = o32018.reindex(r)
        #o32018['O3_obs'] = o32018['O3_obs'].rolling(8, min_periods=6).mean()
        #o32018['O3_ap'] = o32018['O3_ap'].rolling(8, min_periods=6).mean()
        #o32018['O3_pred'] = o32018['O3_pred'].rolling(8, min_periods=6).mean()
        o32018['O3avg8hr_org'] = o32018['O3avg8hr'].shift(-7)
        o32018['O3avg8hr_RF'] = o32018['O3_pred'].shift(-7)
        o32018['O3avg8hr_ap'] = o32018['O3avg8hr_ap'].shift(-7)
        o32018['O3_obs.maxdaily8hravg'] = o32018['O3avg8hr_org'].rolling(17, min_periods=13).max()
        o32018['O3_pred.maxdaily8hravg'] = o32018['O3avg8hr_RF'].rolling(17, min_periods=13).max() 
        o32018['O3_ap.maxdaily8hravg'] = o32018['O3avg8hr_ap'].rolling(17, min_periods=13).max()
        
        #shift columns
        o32018['O3_obs.maxdaily8hravg'] = o32018['O3_obs.maxdaily8hravg'].shift(-16)
        o32018['O3_pred.maxdaily8hravg'] = o32018['O3_pred.maxdaily8hravg'].shift(-16)
        o32018['O3_ap.maxdaily8hravg'] = o32018['O3_ap.maxdaily8hravg'].shift(-16)
        df2018dailyO38hrmax = o32018[(o32018.index.hour == 7)]#.dropna(how='all')
        
        df2018dailyO38hrmax['AQI_day'] = pd.cut(round(df2018dailyO38hrmax['O3_obs.maxdaily8hravg']),
                                [0, 54, 70, 85, 105, 200, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
        df2018dailyO38hrmax['AQI_pred_day'] = pd.cut(round(df2018dailyO38hrmax['O3_pred.maxdaily8hravg']),
                                [0, 54, 70, 85, 105, 200, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
        df2018dailyO38hrmax['AQI_ap_day'] = pd.cut(round(df2018dailyO38hrmax['O3_ap.maxdaily8hravg']),
                                [0, 54, 70, 85, 105, 200, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
    
        df2018dailyO38hrmax.index = df2018dailyO38hrmax.index+pd.DateOffset(hours=5)
        
        aqi_low=[0,51,101,151,201,301]
        aqi_high=[50,100,150,200,300,500]
        aqi_lowc=[0,55,71,86,106,201]
        aqi_highc=[54,70,85,105,200,600]
        df2018dailyO38hrmax['AQI']=np.nan
        for i in range(len(df2018dailyO38hrmax)):
            if(np.isnan(df2018dailyO38hrmax['O3_pred.maxdaily8hravg'][i])): continue
            #df2018dailyO38hrmax['AQI'][i] = aqi.to_iaqi(aqi.POLLUTANT_O3_8H, df2018dailyO38hrmax['O3_pred.maxdaily8hravg'][i]/1000, algo=aqi.ALGO_EPA)
            aqi_class=df2018dailyO38hrmax['AQI_pred_day'][i]-1
            df2018dailyO38hrmax['AQI'][i]=(aqi_high[aqi_class]-aqi_low[aqi_class])/(aqi_highc[aqi_class]-aqi_lowc[aqi_class])*(round(df2018dailyO38hrmax['O3_pred.maxdaily8hravg'][i])-aqi_lowc[aqi_class])+aqi_low[aqi_class]
            df2018dailyO38hrmax['AQI'][i]=round(df2018dailyO38hrmax['AQI'][i])
        
        dict_of_max2019[dict_name] = df2018dailyO38hrmax
        a=a+1
    
    for k in dict_of_max2019.keys():
        dict_of_max2019[k]['datetime']=dict_of_max2019[k].index
        
    big_df = pd.concat(dict_of_max2019)[['datetime','O3_obs.maxdaily8hravg','O3_ap.maxdaily8hravg','O3_pred.maxdaily8hravg']]
    big_df = big_df.groupby('datetime').mean().dropna(how='all')
    big_df['AQI_day'] = pd.cut(round(big_df['O3_obs.maxdaily8hravg']),
                            [0, 54, 70, 85, 105, 200, np.inf],
                            labels=[1, 2, 3, 4, 5, 6])
    big_df['AQI_pred_day'] = pd.cut(round(big_df['O3_pred.maxdaily8hravg']),
                            [0, 54, 70, 85, 105, 200, np.inf],
                            labels=[1, 2, 3, 4, 5, 6])
    big_df['AQI_ap_day'] = pd.cut(round(big_df['O3_ap.maxdaily8hravg']),
                            [0, 54, 70, 85, 105, 200, np.inf],
                            labels=[1, 2, 3, 4, 5, 6])
    big_df.to_csv(outputdir+'data_o3_RFc_'+s+'.csv')
