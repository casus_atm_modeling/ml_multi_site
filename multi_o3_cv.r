rm(list = ls())
install.packages('verification', repos='http://cran.us.r-project.org')
library('verification')
library('ggplot2')
library('reshape2')
library('lubridate')
install.packages('ggExtra', repos='http://cran.us.r-project.org')
library('ggExtra')
require('scales')
o3_RFc_files <- list.files(path = '/bigdata/casus/atmos/ML_model/multi_sites_cv',
                           pattern = glob2rx('data_o3_RFc*8hrmax.csv'),
                           full.names = T)
o3_2RF_files <- list.files(path = '/bigdata/casus/atmos/ML_model/multi_sites_cv',
                           pattern = glob2rx('data_o3_2RF*8hrmax.csv'),
                           full.names = T)

for (f in o3_RFc_files){
  site <- strsplit(f, '[_.]')[[1]][7]
  tmp <- read.csv(f)
  tmp$site <- site
  tmp <- tmp[complete.cases(tmp),]
  colnames(tmp)[1] = 'datetime'
  if (exists("o3_RFc")) o3_RFc <- rbind(o3_RFc, tmp)
  else o3_RFc <- tmp
}

for (f in o3_2RF_files){
  site <- strsplit(f, '[_.]')[[1]][7]
  tmp <- read.csv(f)
  tmp$site <- site
  tmp <- tmp[complete.cases(tmp),]
  colnames(tmp)[1] = 'datetime'
  if (exists("o3_2RF")) o3_2RF <- rbind(o3_2RF, tmp)
  else o3_2RF <- tmp
}

o3_RFc$datetime <- as.POSIXlt(o3_RFc$datetime)
o3_2RF$datetime <- as.POSIXlt(o3_2RF$datetime)

#o3_RFc <- o3_RFc[o3_RFc$datetime < '2020-09-18',]
#o3_2RF <- o3_2RF[o3_2RF$datetime < '2020-09-18',]

df <- merge(o3_RFc[,c(1:4,8)],o3_2RF[,c(1,4,8)],
            by=c('datetime','site'))
df <- na.omit(df)
df$datetime <- paste(date(as.POSIXlt(df$datetime)),df$site)
colnames(df) <- c('datetime','site','Obs','AP','ML1','ML2')
df$Combined <- df$ML2
df[df$ML1>70,'Combined'] <- df[df$ML1>70,'ML1']

stat_RFc <- data.frame(matrix(NA, ncol = 5))
colnames(stat_RFc) <- c('Site','AP','ML1','ML2','Combined')
for (s in 1:length(unique(df$site))){
  tmp <- df[df$site == unique(df$site)[s],]
  stat_RFc[s,'Site'] <- unique(df$site)[s]
  stat_RFc[s,'ML1'] <- sum(tmp$ML1 - tmp$Obs,na.rm = T)/
    sum(tmp$Obs,na.rm = T)*100
  stat_RFc[s,'AP'] <- sum(tmp$AP - tmp$Obs,na.rm = T)/
    sum(tmp$Obs,na.rm = T)*100
  stat_RFc[s,'ML2'] <- sum(tmp$ML2 - tmp$Obs,na.rm = T)/
    sum(tmp$Obs,na.rm = T)*100
  stat_RFc[s,'Combined'] <- sum(tmp$Combined - tmp$Obs,na.rm = T)/
    sum(tmp$Obs,na.rm = T)*100
}

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_nmb_O3.jpeg", units="in", width=5, height=5, res=100)
a<-ggplot()+
  geom_boxplot(data=melt(stat_RFc,id='Site'), aes_string(x='variable', y='value', group = 'variable'),outlier.shape=NA)+
  ylab('NMB (%)') +
  xlab('')+
  theme(legend.position = 'right',legend.title=element_text(size=12),
        axis.text=element_text(size=15),
        axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=12),
        plot.title = element_text(hjust = 0.5,size=20,face="bold"),
        strip.text.x = element_text(size=15),
        strip.background = element_rect(colour=NA, fill=NA))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1))
print(a)
dev.off()


subdf <- df[df$Obs>sort(df$Obs,decreasing = TRUE)[11],]
subdf <- subdf[order(subdf$Obs, decreasing = TRUE),]
subdf <- subdf[c('datetime','Obs','AP','ML1','ML2')]
subdf$datetime <- factor(subdf$datetime, levels = subdf$datetime)
tmp <- melt(subdf[,1:5],id.vars = 'datetime')

df$Year <- year(df$datetime)
jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/obs.jpeg", units="in", width=5, height=5, res=100)
a<-ggplot(data=df,aes(x=as.factor(Year),y=Obs))+
  geom_boxplot()+
  xlab('Year')+
  ylab('Obs. MDA8_O3 (ppb)')+
  #coord_cartesian(ylim = c(0, 20))+
  #coord_cartesian(ylim = c(0, 25))+
  theme(legend.position = c(0.7,0.22),legend.title=element_text(size=15),
        axis.text=element_text(size=20),
        axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=18),
        plot.title = element_text(hjust = 0.5,size=20,face="bold"),
        strip.text.x = element_text(size=20),
        strip.background = element_rect(colour=NA, fill=NA))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1))

print(a)
dev.off()

obs <- merge(o3_RFc[,c(1:5,8)],o3_2RF[,c(1,4,8)],
             by=c('datetime','site'))
obs <- na.omit(obs) 
colnames(obs) <- c('datetime','site','Obs','AP','ML1','AQI','ML2')
obs$Year <- year(obs$datetime)
summary_obs <- data.frame(matrix(data=NA,nrow=4,ncol=7))
colnames(summary_obs) <- c('Year','site_num','mean','aqi1','aqi2','aqi3','aqi4')
summary_obs[,c(1,3)] <- round(aggregate(obs$Obs, by=list(Category=obs$Year), FUN=mean),1)

for (i in 2017:2020){
  tmp <- obs[year(obs$datetime)==i,]
  summary_obs[i-2016,'site_num'] <- length(unique(tmp$site))
  for (q in 1:4){
    summary_obs[i-2016,q+3] <- nrow(tmp[tmp$AQI==q,])
  }
}

# jpeg("/bigdata/casus/atmos/ML_model/multi_top10_O3.jpeg", units="in", width=12, height=5, res=100)
# a<-ggplot(tmp,aes(x=datetime,y=value,fill=variable)) +
#   geom_bar(stat="identity",size=1.2, position=position_dodge())+  
#   geom_text(aes(label=round(as.numeric(tmp$value))),size=5,vjust=-0.25,
#             position=position_dodge(width=0.9))+
#   scale_fill_manual('Legend',
#                     values=c('Obs'="black",'AP'="red",'ML1'="green",'ML2'="blue"),
#                     breaks=c('Obs','AP','ML1','ML2')) +
#   ylab('MDA8 (ppb)')+
#   xlab('')+
#   theme(legend.position = 'right',legend.title=element_text(size=20),
#         axis.text=element_text(size=20),
#         axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=20),
#         plot.title = element_text(hjust = 0.5,size=20,face="bold"),
#         strip.text.x = element_text(size=20),
#         strip.background = element_rect(colour=NA, fill=NA),
#         #axis.text.x=element_blank(),axis.ticks.x=element_blank()
#         axis.text.x=element_blank() #element_text(size=20,angle = 45,hjust=1)
#   )+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# print(a)
# dev.off()

library(MASS)
library(viridis)
get_density <- function(x, y, ...) {
  dens <- MASS::kde2d(x, y, ...)
  ix <- findInterval(x, dens$x)
  iy <- findInterval(y, dens$y)
  ii <- cbind(ix, iy)
  return(dens$z[ii])
}
lmap = lm(AP~Obs, data = df) #Create a linear regression with two variables
r2 = round(summary(lmap)$adj.r.squared, 2)
nmb = round(sum(df$AP - df$Obs,na.rm = T)/sum(df$Obs,na.rm = T)*100,0)
nme = round(sum(abs(df$AP - df$Obs),na.rm = T)/sum(df$Obs,na.rm = T)*100,0)

df$density <- get_density(df$Obs, df$AP, n = 100)
a<-ggplot(df)+
  geom_point(aes(x=Obs,y=AP,color=density))+
  geom_abline(intercept = 0, slope = 1, size=1, col='red')+
  geom_abline(intercept = 70, slope = 0, size=1, col='blue')+
  geom_vline(xintercept = 70, color = "blue", size=1)+
  #ggtitle("(a) AP")+
  xlab('Obs. MDA8 (ppb)')+
  ylab('AIRPACT MDA8 (ppb)')+
  xlim(0,150)+
  ylim(0,150)+
  theme(legend.position = 'none',legend.title=element_text(size=20),
        axis.text=element_text(size=20),
        axis.title=element_text(size=25,face="bold"), legend.text=element_text(size=20),
        plot.title = element_text(hjust = 0.5,size=25,face="bold"),
        strip.text.x = element_text(size=20),
        strip.background = element_rect(colour=NA, fill=NA))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1))+
  annotate("text", x = 80, y=15, size = 6, hjust=0, col='red',
           label = paste("R2 : ",r2,"\nNMB: ",nmb, "%\nNME: ",nme, "%", sep=''))

a <- ggMarginal(a, type="density")
jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_scatter_AP.jpeg", units="in", width=5, height=5, res=100)
print(a)
dev.off()

lmap = lm(ML1~Obs, data = df) #Create a linear regression with two variables
r2 = round(summary(lmap)$adj.r.squared, 2)
nmb = round(sum(df$ML1 - df$Obs,na.rm = T)/sum(df$Obs,na.rm = T)*100,1)
nme = round(sum(abs(df$ML1 - df$Obs),na.rm = T)/sum(df$Obs,na.rm = T)*100,0)

df$density <- get_density(df$Obs, df$ML1, n = 100)
b<-ggplot(df)+
  geom_point(aes(x=Obs,y=ML1,color=density))+
  geom_abline(intercept = 0, slope = 1, size=1, col='red')+
  geom_abline(intercept = 70, slope = 0, size=1, col='blue')+
  geom_vline(xintercept = 70, color = "blue", size=1)+
  #ggtitle("(b) ML1")+
  xlab('Obs. MDA8 (ppb)')+
  ylab('ML1 MDA8 (ppb)')+
  xlim(0,150)+
  ylim(0,150)+
  theme(legend.position = 'none',legend.title=element_text(size=20),
        axis.text=element_text(size=20),
        axis.title=element_text(size=25,face="bold"), legend.text=element_text(size=20),
        plot.title = element_text(hjust = 0.5,size=25,face="bold"),
        strip.text.x = element_text(size=20),
        strip.background = element_rect(colour=NA, fill=NA))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1))+
  annotate("text", x = 80, y=15, size = 6, hjust=0, col='red',
           label = paste("R2 : ",r2,"\nNMB: ",nmb, "%\nNME: ",nme, "%", sep=''))

b <- ggMarginal(b, type="density")
jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_scatter_ML1.jpeg", units="in", width=5, height=5, res=100)
print(b)
dev.off()

lmap = lm(ML2~Obs, data = df) #Create a linear regression with two variables
r2 = round(summary(lmap)$adj.r.squared, 2)
nmb = round(sum(df$ML2 - df$Obs,na.rm = T)/sum(df$Obs,na.rm = T)*100,2)
nme = round(sum(abs(df$ML2 - df$Obs),na.rm = T)/sum(df$Obs,na.rm = T)*100,0)

df$density <- get_density(df$Obs, df$ML2, n = 100)
c<-ggplot(df)+
  geom_point(aes(x=Obs,y=ML2,color=density))+
  geom_abline(intercept = 0, slope = 1, size=1, col='red')+
  geom_abline(intercept = 70, slope = 0, size=1, col='blue')+
  geom_vline(xintercept = 70, color = "blue", size=1)+
  #ggtitle("(c) ML2")+
  xlab('Obs. MDA8 (ppb)')+
  ylab('ML2 MDA8 (ppb)')+
  xlim(0,150)+
  ylim(0,150)+
  theme(legend.position = 'none',legend.title=element_text(size=20),
        axis.text=element_text(size=20),
        axis.title=element_text(size=25,face="bold"), legend.text=element_text(size=20),
        plot.title = element_text(hjust = 0.5,size=25,face="bold"),
        strip.text.x = element_text(size=20),
        strip.background = element_rect(colour=NA, fill=NA))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1))+
  annotate("text", x = 80, y=15, size = 6, hjust=0, col='red',
           label = paste("R2 : ",r2,"\nNMB: ",nmb, "%\nNME: ",nme, "%", sep=''))

c <- ggMarginal(c, type="density")
jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_scatter_ML2.jpeg", units="in", width=5, height=5, res=100)
print(c)
dev.off()

lmap = lm(Combined~Obs, data = df) #Create a linear regression with two variables
r2 = round(summary(lmap)$adj.r.squared, 2)
nmb = round(sum(df$Combined - df$Obs,na.rm = T)/sum(df$Obs,na.rm = T)*100,2)
nme = round(sum(abs(df$Combined - df$Obs),na.rm = T)/sum(df$Obs,na.rm = T)*100,0)

df$density <- get_density(df$Obs, df$Combined, n = 100)
d<-ggplot(df)+
  geom_point(aes(x=Obs,y=Combined,color=density))+
  geom_abline(intercept = 0, slope = 1, size=1, col='red')+
  geom_abline(intercept = 70, slope = 0, size=1, col='blue')+
  geom_vline(xintercept = 70, color = "blue", size=1)+
  #ggtitle("(c) ML2")+
  xlab('Obs. MDA8 (ppb)')+
  ylab('Combined MDA8 (ppb)')+
  xlim(0,150)+
  ylim(0,150)+
  theme(legend.position = 'none',legend.title=element_text(size=20),
        axis.text=element_text(size=20),
        axis.title=element_text(size=25,face="bold"), legend.text=element_text(size=20),
        plot.title = element_text(hjust = 0.5,size=25,face="bold"),
        strip.text.x = element_text(size=20),
        strip.background = element_rect(colour=NA, fill=NA))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1))+
  annotate("text", x = 80, y=15, size = 6, hjust=0, col='red',
           label = paste("R2 : ",r2,"\nNMB: ",nmb, "%\nNME: ",nme, "%", sep=''))

d <- ggMarginal(d, type="density")
jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_scatter_Combined.jpeg", units="in", width=5, height=5, res=100)
print(d)
dev.off()
# jpeg("/bigdata/casus/atmos/ML_model/multi_scatter_O3.jpeg", units="in", width=15, height=5, res=100)
# library(ggpubr)
# a<-ggarrange(a, b, c, 
#              #labels = c("ML1", "ML2", "AP"),
#              ncol = 3, nrow = 1)
# print(a)
# dev.off()

a = which.max(density(df$Obs)$y)
density(df$Obs)$x[a]
a = which.max(density(df$AP)$y)
density(df$AP)$x[a]
a = which.max(density(df$ML1)$y)
density(df$ML1)$x[a]
MaxY<- max(density(df$ML1)$y[density(df$ML1)$x > 40])
a = which(density(df$ML1)$y == MaxY)
density(df$ML1)$x[a]
a = which.max(density(df$ML2)$y)
density(df$ML2)$x[a]
a = which.max(density(df$Combined)$y)
density(df$Combined)$x[a]

#nmb----
site_info <- read.csv('/Users/fankai/OneDrive - Washington State University (email.wsu.edu)/ML/Unique_monitoring_sites_and_locations_for_all_pollutants.csv', stringsAsFactors=FALSE)
site_info$AQS_ID <- formatC(as.numeric(site_info$AQS_ID), width = 9, format = "d", flag = "0")
df <- merge(df,site_info,by.x='site',by.y='AQS_ID')
sites <- unique(df$site)

nmb_table <- data.frame(matrix(NA,ncol=6))
colnames(nmb_table) <- c('Sites','Year','AIRPACT','ML1','ML2','Combined')

for (s in 1:length(sites)){
  sub_df <- df[df$site==sites[s],]
  for (i in 2017:2020){
    tmp <- sub_df[year(df$datetime)==i,]
    nmb_table[s*4+i-2020,'Sites'] <- sites[s]
    nmb_table[s*4+i-2020,'Year'] <- i
    nmb_table[s*4+i-2020,'AIRPACT'] <- round(sum(tmp$AP - tmp$Obs,na.rm = T)/
                                               sum(tmp[complete.cases(tmp$AP),]$Obs,na.rm = T)*100,1)
    nmb_table[s*4+i-2020,'ML1'] <- round(sum(tmp$ML1 - tmp$Obs,na.rm = T)/
                                           sum(tmp[complete.cases(tmp$ML1),]$Obs,na.rm = T)*100,1)
    nmb_table[s*4+i-2020,'ML2'] <- round(sum(tmp$ML2 - tmp$Obs,na.rm = T)/
                                           sum(tmp[complete.cases(tmp$ML2),]$Obs,na.rm = T)*100,1)
    nmb_table[s*4+i-2020,'Combined'] <- round(sum(tmp$Combined - tmp$Obs,na.rm = T)/
                                           sum(tmp[complete.cases(tmp$Combined),]$Obs,na.rm = T)*100,1)
  }
}

library(reshape2)
a<-ggplot(data=melt(nmb_table, id.vars = c('Sites','Year')),aes(x=as.factor(Year),y=value,fill=variable))+
  geom_boxplot()+
  scale_fill_manual('Legend',
                    values=c('AIRPACT'="red",'ML1'="green",'ML2'="blue",'Combined'='purple'),
                    breaks=c('AIRPACT','ML1','ML2','Combined')) +
  xlab('Year')+
  ylab('NMB (%)')+
  #coord_cartesian(ylim = c(-70, 70))+
  theme(legend.position = 'right',legend.title=element_text(size=15),
        axis.text=element_text(size=20),
        axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=18),
        plot.title = element_text(hjust = 0.5,size=20,face="bold"),
        strip.text.x = element_text(size=20),
        strip.background = element_rect(colour=NA, fill=NA))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1))

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/model_nmb_O3.jpeg", units="in", width=8, height=5, res=100)
print(a)
dev.off()

# #ratio----
# df_aqi <- data.frame(xmin=c(1,54.5,70.5,85.5),
#                      xmax=c(54.5,70.5,85.5,Inf),
#                      ymin=c(0.1,0.1,0.1,0.1),
#                      ymax=c(Inf,Inf,Inf,Inf),
#                      AQI_category=c("Good","Moderate","Unhealthy for sensitive groups","Unhealthy"))
# 
# df$APobs <- df$AP/df$Obs
# df$ML1obs <- df$ML1/df$Obs
# df$ML2obs <- df$ML2/df$Obs
# a<-ggplot(data=df)+
#   geom_point(aes(x=Obs,y=APobs))+
#   geom_hline(yintercept=1, linetype="dashed", color = "red", size=1.5)+
#   geom_rect(data=df_aqi,aes(xmin=xmin,ymin=ymin,xmax=xmax,ymax=ymax,fill=AQI_category),
#             alpha=0.5,inherit.aes=FALSE)+
#   scale_fill_manual(breaks=c("Good","Moderate","Unhealthy for sensitive groups","Unhealthy"),
#                     values=c("green","yellow","orange","red"),
#                     labels=c("Good","Moderate","Unhealthy for sensitive groups","Unhealthy"))+
#   xlab('Obs (ppb)')+
#   ylab('AIRPACT/Obs')+
#   scale_x_continuous(trans = 'log10',breaks=c(1,10,100), limits=c(1,120)) +
#   scale_y_continuous(trans = 'log10',breaks=c(1,10,100), limits=c(0.1,120)) +
#   theme(legend.position = 'none',legend.title=element_text(size=12),
#         axis.text=element_text(size=12),
#         axis.title=element_text(size=12,face="bold"), legend.text=element_text(size=12),
#         plot.title = element_text(hjust = 0.5,size=12,face="bold"),
#         strip.text.x = element_text(size=12),
#         strip.background = element_rect(colour=NA, fill=NA))+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_ratio_AP.jpeg", units="in", width=6, height=5, res=100)
# print(a)
# dev.off()
# 
# a<-ggplot(data=df)+
#   geom_point(aes(x=Obs,y=ML1obs))+
#   geom_hline(yintercept=1, linetype="dashed", color = "red", size=1.5)+
#   geom_rect(data=df_aqi,aes(xmin=xmin,ymin=ymin,xmax=xmax,ymax=ymax,fill=AQI_category),
#             alpha=0.5,inherit.aes=FALSE)+
#   scale_fill_manual(breaks=c("Good","Moderate","Unhealthy for sensitive groups","Unhealthy"),
#                     values=c("green","yellow","orange","red"),
#                     labels=c("Good","Moderate","Unhealthy for sensitive groups","Unhealthy"))+
#   xlab('Obs (ppb)')+
#   ylab('ML1/Obs')+
#   scale_x_continuous(trans = 'log10',breaks=c(1,10,100), limits=c(1,120)) +
#   scale_y_continuous(trans = 'log10',breaks=c(1,10,100), limits=c(0.1,120)) +
#   theme(legend.position = 'none',legend.title=element_text(size=12),
#         axis.text=element_text(size=12),
#         axis.title=element_text(size=12,face="bold"), legend.text=element_text(size=12),
#         plot.title = element_text(hjust = 0.5,size=12,face="bold"),
#         strip.text.x = element_text(size=12),
#         strip.background = element_rect(colour=NA, fill=NA))+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_ratio_ML1.jpeg", units="in", width=6, height=5, res=100)
# print(a)
# dev.off()
# 
# a<-ggplot(data=df)+
#   geom_point(aes(x=Obs,y=ML2obs))+
#   geom_hline(yintercept=1, linetype="dashed", color = "red", size=1.5)+
#   geom_rect(data=df_aqi,aes(xmin=xmin,ymin=ymin,xmax=xmax,ymax=ymax,fill=AQI_category),
#             alpha=0.5,inherit.aes=FALSE)+
#   scale_fill_manual(breaks=c("Good","Moderate","Unhealthy for sensitive groups","Unhealthy"),
#                     values=c("green","yellow","orange","red"),
#                     labels=c("Good","Moderate","Unhealthy for sensitive groups","Unhealthy"))+
#   xlab('Obs (ppb)')+
#   ylab('ML2/Obs')+
#   scale_x_continuous(trans = 'log10',breaks=c(1,10,100), limits=c(1,120)) +
#   scale_y_continuous(trans = 'log10',breaks=c(1,10,100), limits=c(0.1,120)) +
#   theme(legend.position = 'none',legend.title=element_text(size=12),
#         axis.text=element_text(size=12),
#         axis.title=element_text(size=12,face="bold"), legend.text=element_text(size=12),
#         plot.title = element_text(hjust = 0.5,size=12,face="bold"),
#         strip.text.x = element_text(size=12),
#         strip.background = element_rect(colour=NA, fill=NA))+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_ratio_ML2.jpeg", units="in", width=6, height=5, res=100)
# print(a)
# dev.off()

#ml1----
df <- merge(o3_RFc,o3_2RF[,c(1,4,6,8)],
            by=c('datetime','site'))
df <- na.omit(df)
colnames(df) <- c('datetime','site','Obs','AP','ML1','AQI_day','ML1_AQI','AP_AQI','ML2','ML2_AQI')
df$Combined <- df$ML2
df$Combined_AQI <- df$ML2_AQI
df[df$ML1>70,'Combined'] <- df[df$ML1>70,'ML1']
df[df$ML1>70,'Combined_AQI'] <- df[df$ML1>70,'ML1_AQI']

#site_info <- read.csv('/Users/fankai/OneDrive - Washington State University (email.wsu.edu)/ML/Unique_monitoring_sites_and_locations_for_all_pollutants.csv', stringsAsFactors=FALSE)
#site_info$AQS_ID <- formatC(as.numeric(site_info$AQS_ID), width = 9, format = "d", flag = "0")
#df <- aggregate(. ~ datetime + site, data = df, mean, na.rm = TRUE)
df$datetime <- as.POSIXct(df$datetime)
df <- df[month(df$datetime)>5&month(df$datetime)<9,]
sites <- unique(df$site)
stat_table <- data.frame(matrix(NA,nrow=length(unique(df$aqsid))*3,ncol=6))
colnames(stat_table) <- c('AQS_ID','mod','obs_1','obs_2','obs_3','obs_4')
hss_kss <- data.frame(matrix(NA,nrow=length(unique(df$aqsid)),ncol=19))
colnames(hss_kss) <- c('AQS_ID','ML1_hss','ML1_kss','ML1_ts1','ML1_ts2','ML1_ts3','ML1_ts4',
                       'OBS_mean','ML1_NMB','ML1_NMB1','ML1_NMB2','ML1_NMB3','ML1_NMB4',
                       'OBS_AQI3_days','ML1_AQI3_days_right','ML1_AQI3_days_wrong',
                       'OBS_AQI4_days','ML1_AQI4_days_right','ML1_AQI4_days_wrong')
for (s in 1:length(sites)){
  sub_df <- df[df$site==sites[s],]
  sub_df <- sub_df[complete.cases(sub_df),]
  for (i in 1:4){
    stat_table[s*4-4+i,'AQS_ID'] <- sites[s]
    stat_table[s*4-4+i,'mod'] <- i
    stat_table[s*4-4+i,'obs_1'] <- nrow(sub_df[sub_df$AQI_day==1&sub_df$ML1_AQI==i,])
    stat_table[s*4-4+i,'obs_2'] <- nrow(sub_df[sub_df$AQI_day==2&sub_df$ML1_AQI==i,])
    stat_table[s*4-4+i,'obs_3'] <- nrow(sub_df[sub_df$AQI_day==3&sub_df$ML1_AQI==i,])
    stat_table[s*4-4+i,'obs_4'] <- nrow(sub_df[sub_df$AQI_day==4&sub_df$ML1_AQI==i,])
  }
  
  aqi_table = na.omit(stat_table[stat_table$AQS_ID == sites[s],c('obs_1','obs_2','obs_3','obs_4')])
  hss_kss[s,'AQS_ID'] <- sites[s]
  hss_kss[s,'ML1_hss'] <- multi.cont(as.matrix(aqi_table))$hss
  hss_kss[s,'ML1_kss'] <- multi.cont(as.matrix(aqi_table))$pss
  hss_kss[s,'ML1_ts1'] <- multi.cont(as.matrix(aqi_table))$ts[1]
  hss_kss[s,'ML1_ts2'] <- multi.cont(as.matrix(aqi_table))$ts[2]
  hss_kss[s,'ML1_ts3'] <- multi.cont(as.matrix(aqi_table))$ts[3]
  hss_kss[s,'ML1_ts4'] <- multi.cont(as.matrix(aqi_table))$ts[4]
  hss_kss[s,'OBS_AQI3_days'] <- nrow(sub_df[sub_df$AQI_day==3,])
  hss_kss[s,'ML1_AQI3_days_right'] <- nrow(sub_df[sub_df$AQI_day==3&sub_df$ML1_AQI==3,])
  hss_kss[s,'ML1_AQI3_days_wrong'] <- nrow(sub_df[sub_df$AQI_day!=3&sub_df$ML1_AQI==3,])
  hss_kss[s,'OBS_AQI4_days'] <- nrow(sub_df[sub_df$AQI_day==4,])
  hss_kss[s,'ML1_AQI4_days_right'] <- nrow(sub_df[sub_df$AQI_day==4&sub_df$ML1_AQI==4,])
  hss_kss[s,'ML1_AQI4_days_wrong'] <- nrow(sub_df[sub_df$AQI_day!=4&sub_df$ML1_AQI==4,])
  hss_kss[s,'OBS_mean'] <- mean(sub_df$Obs)
  hss_kss[s,'ML1_NMB'] <- sum(sub_df$ML1 - sub_df$Obs,na.rm = T)/
    sum(sub_df$Obs,na.rm = T)*100
  if (nrow(sub_df[sub_df$AQI_day==1,]) > 0) {hss_kss[s,'ML1_NMB1'] <- 
    sum(sub_df[sub_df$AQI_day==1,]$ML1 - sub_df[sub_df$AQI_day==1,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==1,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'ML1_NMB1'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==2,]) > 0) {hss_kss[s,'ML1_NMB2'] <- 
    sum(sub_df[sub_df$AQI_day==2,]$ML1 - sub_df[sub_df$AQI_day==2,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==2,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'ML1_NMB2'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==3,]) > 0) {hss_kss[s,'ML1_NMB3'] <- 
    sum(sub_df[sub_df$AQI_day==3,]$ML1 - sub_df[sub_df$AQI_day==3,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==3,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'ML1_NMB3'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==4,]) > 0) {hss_kss[s,'ML1_NMB4'] <- 
    sum(sub_df[sub_df$AQI_day==4,]$ML1 - sub_df[sub_df$AQI_day==4,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==4,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'ML1_NMB4'] <- NA}
}
stat_table <- merge(stat_table,site_info,by='AQS_ID')
hss_kss_ml1 <- merge(hss_kss,site_info,by='AQS_ID')
hss_kss_ml1 <- hss_kss_ml1[order(hss_kss_ml1$OBS_AQI3_days),]
hss_kss_ml1$Sitename <- factor(hss_kss_ml1$Sitename, levels = hss_kss_ml1$Sitename)

#airpact----
stat_table <- data.frame(matrix(NA,nrow=length(unique(df$site))*3,ncol=6))
colnames(stat_table) <- c('AQS_ID','mod','obs_1','obs_2','obs_3','obs_4')
hss_kss <- data.frame(matrix(NA,nrow=length(unique(df$site)),ncol=19))
colnames(hss_kss) <- c('AQS_ID','AP_hss','AP_kss','AP_ts1','AP_ts2','AP_ts3','AP_ts4',
                       'OBS_mean','AP_NMB','AP_NMB1','AP_NMB2','AP_NMB3','AP_NMB4',
                       'OBS_AQI3_days','AP_AQI3_days_right','AP_AQI3_days_wrong',
                       'OBS_AQI4_days','AP_AQI4_days_right','AP_AQI4_days_wrong')
for (s in 1:length(sites)){
  sub_df <- df[df$site==sites[s],]
  sub_df <- sub_df[complete.cases(sub_df),]
  for (i in 1:4){
    stat_table[s*4-4+i,'AQS_ID'] <- sites[s]
    stat_table[s*4-4+i,'mod'] <- i
    stat_table[s*4-4+i,'obs_1'] <- nrow(sub_df[sub_df$AQI_day==1&sub_df$AP_AQI==i,])
    stat_table[s*4-4+i,'obs_2'] <- nrow(sub_df[sub_df$AQI_day==2&sub_df$AP_AQI==i,])
    stat_table[s*4-4+i,'obs_3'] <- nrow(sub_df[sub_df$AQI_day==3&sub_df$AP_AQI==i,])
    stat_table[s*4-4+i,'obs_4'] <- nrow(sub_df[sub_df$AQI_day==4&sub_df$AP_AQI==i,])
  }
  
  aqi_table = na.omit(stat_table[stat_table$AQS_ID == sites[s],c('obs_1','obs_2','obs_3','obs_4')])
  hss_kss[s,'AQS_ID'] <- sites[s]
  hss_kss[s,'AP_hss'] <- multi.cont(as.matrix(aqi_table))$hss
  hss_kss[s,'AP_kss'] <- multi.cont(as.matrix(aqi_table))$pss
  hss_kss[s,'AP_ts1'] <- multi.cont(as.matrix(aqi_table))$ts[1]
  hss_kss[s,'AP_ts2'] <- multi.cont(as.matrix(aqi_table))$ts[2]
  hss_kss[s,'AP_ts3'] <- multi.cont(as.matrix(aqi_table))$ts[3]
  hss_kss[s,'AP_ts4'] <- multi.cont(as.matrix(aqi_table))$ts[4]
  hss_kss[s,'OBS_AQI3_days'] <- nrow(sub_df[sub_df$AQI_day==3,])
  hss_kss[s,'AP_AQI3_days_right'] <- nrow(sub_df[sub_df$AQI_day==3&sub_df$AP_AQI==3,])
  hss_kss[s,'AP_AQI3_days_wrong'] <- nrow(sub_df[sub_df$AQI_day!=3&sub_df$AP_AQI==3,])
  hss_kss[s,'OBS_AQI4_days'] <- nrow(sub_df[sub_df$AQI_day==4,])
  hss_kss[s,'AP_AQI4_days_right'] <- nrow(sub_df[sub_df$AQI_day==4&sub_df$AP_AQI==4,])
  hss_kss[s,'AP_AQI4_days_wrong'] <- nrow(sub_df[sub_df$AQI_day!=4&sub_df$AP_AQI==4,])
  hss_kss[s,'OBS_mean'] <- mean(sub_df$Obs)
  hss_kss[s,'AP_NMB'] <- sum(sub_df$AP - sub_df$Obs,na.rm = T)/
    sum(sub_df$Obs,na.rm = T)*100
  if (nrow(sub_df[sub_df$AQI_day==1,]) > 0) {hss_kss[s,'AP_NMB1'] <- 
    sum(sub_df[sub_df$AQI_day==1,]$AP - sub_df[sub_df$AQI_day==1,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==1,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'AP_NMB1'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==2,]) > 0) {hss_kss[s,'AP_NMB2'] <- 
    sum(sub_df[sub_df$AQI_day==2,]$AP - sub_df[sub_df$AQI_day==2,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==2,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'AP_NMB2'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==3,]) > 0) {hss_kss[s,'AP_NMB3'] <- 
    sum(sub_df[sub_df$AQI_day==3,]$AP - sub_df[sub_df$AQI_day==3,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==3,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'AP_NMB3'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==4,]) > 0) {hss_kss[s,'AP_NMB4'] <- 
    sum(sub_df[sub_df$AQI_day==4,]$AP - sub_df[sub_df$AQI_day==4,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==4,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'AP_NMB4'] <- NA}
}
stat_table <- merge(stat_table,site_info,by='AQS_ID')
hss_kss_ap <- merge(hss_kss,site_info,by='AQS_ID')
hss_kss_ap <- hss_kss_ap[order(hss_kss_ap$OBS_AQI3_days),]
hss_kss_ap$Sitename <- factor(hss_kss_ap$Sitename, levels = hss_kss_ap$Sitename)

#ml2----
#site_info <- read.csv('/Users/fankai/OneDrive - Washington State University (email.wsu.edu)/ML/Unique_monitoring_sites_and_locations_for_all_pollutants.csv', stringsAsFactors=FALSE)
#site_info$AQS_ID <- formatC(as.numeric(site_info$AQS_ID), width = 9, format = "d", flag = "0")
#df <- aggregate(. ~ datetime + site, data = o3_2RF, mean, na.rm = TRUE)
#df$datetime <- as.POSIXct(df$datetime)
#df <- df[month(df$datetime)>5&month(df$datetime)<9,]
#sites <- unique(df$site)
stat_table <- data.frame(matrix(NA,nrow=length(unique(df$site))*3,ncol=6))
colnames(stat_table) <- c('AQS_ID','mod','obs_1','obs_2','obs_3','obs_4')
hss_kss <- data.frame(matrix(NA,nrow=length(unique(df$site)),ncol=19))
colnames(hss_kss) <- c('AQS_ID','ML2_hss','ML2_kss','ML2_ts1','ML2_ts2','ML2_ts3','ML2_ts4',
                       'OBS_mean','ML2_NMB','ML2_NMB1','ML2_NMB2','ML2_NMB3','ML2_NMB4',
                       'OBS_AQI3_days','ML2_AQI3_days_right','ML2_AQI3_days_wrong',
                       'OBS_AQI4_days','ML2_AQI4_days_right','ML2_AQI4_days_wrong')
for (s in 1:length(sites)){
  sub_df <- df[df$site==sites[s],]
  sub_df <- sub_df[complete.cases(sub_df),]
  for (i in 1:4){
    stat_table[s*4-4+i,'AQS_ID'] <- sites[s]
    stat_table[s*4-4+i,'mod'] <- i
    stat_table[s*4-4+i,'obs_1'] <- nrow(sub_df[sub_df$AQI_day==1&sub_df$ML2_AQI==i,])
    stat_table[s*4-4+i,'obs_2'] <- nrow(sub_df[sub_df$AQI_day==2&sub_df$ML2_AQI==i,])
    stat_table[s*4-4+i,'obs_3'] <- nrow(sub_df[sub_df$AQI_day==3&sub_df$ML2_AQI==i,])
    stat_table[s*4-4+i,'obs_4'] <- nrow(sub_df[sub_df$AQI_day==4&sub_df$ML2_AQI==i,])
  }
  
  aqi_table = na.omit(stat_table[stat_table$AQS_ID == sites[s],c('obs_1','obs_2','obs_3','obs_4')])
  hss_kss[s,'AQS_ID'] <- sites[s]
  hss_kss[s,'ML2_hss'] <- multi.cont(as.matrix(aqi_table))$hss
  hss_kss[s,'ML2_kss'] <- multi.cont(as.matrix(aqi_table))$pss
  hss_kss[s,'ML2_ts1'] <- multi.cont(as.matrix(aqi_table))$ts[1]
  hss_kss[s,'ML2_ts2'] <- multi.cont(as.matrix(aqi_table))$ts[2]
  hss_kss[s,'ML2_ts3'] <- multi.cont(as.matrix(aqi_table))$ts[3]
  hss_kss[s,'ML2_ts4'] <- multi.cont(as.matrix(aqi_table))$ts[4]
  hss_kss[s,'OBS_AQI3_days'] <- nrow(sub_df[sub_df$AQI_day==3,])
  hss_kss[s,'ML2_AQI3_days_right'] <- nrow(sub_df[sub_df$AQI_day==3&sub_df$ML2_AQI==3,])
  hss_kss[s,'ML2_AQI3_days_wrong'] <- nrow(sub_df[sub_df$AQI_day!=3&sub_df$ML2_AQI==3,])
  hss_kss[s,'OBS_AQI4_days'] <- nrow(sub_df[sub_df$AQI_day==4,])
  hss_kss[s,'ML2_AQI4_days_right'] <- nrow(sub_df[sub_df$AQI_day==4&sub_df$ML2_AQI==4,])
  hss_kss[s,'ML2_AQI4_days_wrong'] <- nrow(sub_df[sub_df$AQI_day!=4&sub_df$ML2_AQI==4,])
  hss_kss[s,'OBS_mean'] <- mean(sub_df$Obs)
  hss_kss[s,'ML2_NMB'] <- sum(sub_df$ML2 - sub_df$Obs,na.rm = T)/
    sum(sub_df$Obs,na.rm = T)*100
  if (nrow(sub_df[sub_df$AQI_day==1,]) > 0) {hss_kss[s,'ML2_NMB1'] <- 
    sum(sub_df[sub_df$AQI_day==1,]$ML2 - sub_df[sub_df$AQI_day==1,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==1,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'ML2_NMB1'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==2,]) > 0) {hss_kss[s,'ML2_NMB2'] <- 
    sum(sub_df[sub_df$AQI_day==2,]$ML2 - sub_df[sub_df$AQI_day==2,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==2,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'ML2_NMB2'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==3,]) > 0) {hss_kss[s,'ML2_NMB3'] <- 
    sum(sub_df[sub_df$AQI_day==3,]$ML2 - sub_df[sub_df$AQI_day==3,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==3,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'ML2_NMB3'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==4,]) > 0) {hss_kss[s,'ML2_NMB4'] <- 
    sum(sub_df[sub_df$AQI_day==4,]$ML2 - sub_df[sub_df$AQI_day==4,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==4,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'ML2_NMB4'] <- NA}
}
stat_table <- merge(stat_table,site_info,by='AQS_ID')
hss_kss_ml2 <- merge(hss_kss,site_info,by='AQS_ID')
hss_kss_ml2 <- hss_kss_ml2[order(hss_kss_ml2$OBS_AQI3_days),]
hss_kss_ml2$Sitename <- factor(hss_kss_ml2$Sitename, levels = hss_kss_ml2$Sitename)

#combined----
stat_table <- data.frame(matrix(NA,nrow=length(unique(df$site))*3,ncol=6))
colnames(stat_table) <- c('AQS_ID','mod','obs_1','obs_2','obs_3','obs_4')
hss_kss <- data.frame(matrix(NA,nrow=length(unique(df$site)),ncol=19))
colnames(hss_kss) <- c('AQS_ID','Combined_hss','Combined_kss','Combined_ts1','Combined_ts2','Combined_ts3','Combined_ts4',
                       'OBS_mean','Combined_NMB','Combined_NMB1','Combined_NMB2','Combined_NMB3','Combined_NMB4',
                       'OBS_AQI3_days','Combined_AQI3_days_right','Combined_AQI3_days_wrong',
                       'OBS_AQI4_days','Combined_AQI4_days_right','Combined_AQI4_days_wrong')
for (s in 1:length(sites)){
  sub_df <- df[df$site==sites[s],]
  sub_df <- sub_df[complete.cases(sub_df),]
  for (i in 1:4){
    stat_table[s*4-4+i,'AQS_ID'] <- sites[s]
    stat_table[s*4-4+i,'mod'] <- i
    stat_table[s*4-4+i,'obs_1'] <- nrow(sub_df[sub_df$AQI_day==1&sub_df$Combined_AQI==i,])
    stat_table[s*4-4+i,'obs_2'] <- nrow(sub_df[sub_df$AQI_day==2&sub_df$Combined_AQI==i,])
    stat_table[s*4-4+i,'obs_3'] <- nrow(sub_df[sub_df$AQI_day==3&sub_df$Combined_AQI==i,])
    stat_table[s*4-4+i,'obs_4'] <- nrow(sub_df[sub_df$AQI_day==4&sub_df$Combined_AQI==i,])
  }
  
  aqi_table = na.omit(stat_table[stat_table$AQS_ID == sites[s],c('obs_1','obs_2','obs_3','obs_4')])
  hss_kss[s,'AQS_ID'] <- sites[s]
  hss_kss[s,'Combined_hss'] <- multi.cont(as.matrix(aqi_table))$hss
  hss_kss[s,'Combined_kss'] <- multi.cont(as.matrix(aqi_table))$pss
  hss_kss[s,'Combined_ts1'] <- multi.cont(as.matrix(aqi_table))$ts[1]
  hss_kss[s,'Combined_ts2'] <- multi.cont(as.matrix(aqi_table))$ts[2]
  hss_kss[s,'Combined_ts3'] <- multi.cont(as.matrix(aqi_table))$ts[3]
  hss_kss[s,'Combined_ts4'] <- multi.cont(as.matrix(aqi_table))$ts[4]
  hss_kss[s,'OBS_AQI3_days'] <- nrow(sub_df[sub_df$AQI_day==3,])
  hss_kss[s,'Combined_AQI3_days_right'] <- nrow(sub_df[sub_df$AQI_day==3&sub_df$Combined_AQI==3,])
  hss_kss[s,'Combined_AQI3_days_wrong'] <- nrow(sub_df[sub_df$AQI_day!=3&sub_df$Combined_AQI==3,])
  hss_kss[s,'OBS_AQI4_days'] <- nrow(sub_df[sub_df$AQI_day==4,])
  hss_kss[s,'Combined_AQI4_days_right'] <- nrow(sub_df[sub_df$AQI_day==4&sub_df$Combined_AQI==4,])
  hss_kss[s,'Combined_AQI4_days_wrong'] <- nrow(sub_df[sub_df$AQI_day!=4&sub_df$Combined_AQI==4,])
  hss_kss[s,'OBS_mean'] <- mean(sub_df$Obs)
  hss_kss[s,'Combined_NMB'] <- sum(sub_df$Combined - sub_df$Obs,na.rm = T)/
    sum(sub_df$Obs,na.rm = T)*100
  if (nrow(sub_df[sub_df$AQI_day==1,]) > 0) {hss_kss[s,'Combined_NMB1'] <- 
    sum(sub_df[sub_df$AQI_day==1,]$Combined - sub_df[sub_df$AQI_day==1,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==1,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'Combined_NMB1'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==2,]) > 0) {hss_kss[s,'Combined_NMB2'] <- 
    sum(sub_df[sub_df$AQI_day==2,]$Combined - sub_df[sub_df$AQI_day==2,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==2,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'Combined_NMB2'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==3,]) > 0) {hss_kss[s,'Combined_NMB3'] <- 
    sum(sub_df[sub_df$AQI_day==3,]$Combined - sub_df[sub_df$AQI_day==3,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==3,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'Combined_NMB3'] <- NA}
  if (nrow(sub_df[sub_df$AQI_day==4,]) > 0) {hss_kss[s,'Combined_NMB4'] <- 
    sum(sub_df[sub_df$AQI_day==4,]$Combined - sub_df[sub_df$AQI_day==4,]$Obs,na.rm = T)/
    sum(sub_df[sub_df$AQI_day==4,]$Obs,na.rm = T)*100
  } else {hss_kss[s,'Combined_NMB4'] <- NA}
}
stat_table <- merge(stat_table,site_info,by='AQS_ID')
hss_kss_Combined <- merge(hss_kss,site_info,by='AQS_ID')
hss_kss_Combined <- hss_kss_Combined[order(hss_kss_Combined$OBS_AQI3_days),]
hss_kss_Combined$Sitename <- factor(hss_kss_Combined$Sitename, levels = hss_kss_Combined$Sitename)

hss_kss_ap_short <- hss_kss_ap[,c("AP_hss","AP_kss","OBS_mean","AP_NMB",'Sitename')]
hss_kss_ml1_short <- hss_kss_ml1[,c("ML1_hss","ML1_kss","ML1_NMB",'Sitename')]
hss_kss_ml2_short <- hss_kss_ml2[,c("ML2_hss","ML2_kss","ML2_NMB",'Sitename')]
hss_kss_Combined_short <- hss_kss_Combined[,c("Combined_hss","Combined_kss","Combined_NMB",'Sitename')]

# #line right----
# hss_kss_ap_short <- hss_kss_ap[(hss_kss_ap$OBS_AQI3_days>0),c('OBS_AQI3_days','AP_AQI3_days_right','Sitename')]
# hss_kss_ml1_short <- hss_kss_ml1[(hss_kss_ml1$OBS_AQI3_days>0),c('OBS_AQI3_days','ML1_AQI3_days_right','Sitename')]
# hss_kss_ml2_short <- hss_kss_ml2[(hss_kss_ml2$OBS_AQI3_days>0),c('OBS_AQI3_days','ML2_AQI3_days_right','Sitename')]
# 
# library(reshape2)
# temp1 <- merge(hss_kss_ap_short,hss_kss_ml1_short)
# temp2 <- merge(temp1,hss_kss_ml2_short)
# colnames(temp2) <- c("OBS","Sitename","AP","ML1","ML2")
# #temp2 <- temp2[order(temp2$OBS),]
# temp2[temp2$AP>0,'AP'] <- temp2[temp2$AP>0,'AP']-0.1
# temp2[temp2$ML2>0,'ML2'] <- temp2[temp2$ML2>0,'ML2']+0.1
# 
# #temp2$Sitename <- factor(temp2$Sitename, levels = temp2$Sitename)
# a<-ggplot(data=melt(temp2),aes(x=Sitename, y=value, group=variable, fill=variable))+
#   geom_bar(stat="identity", position=position_dodge())+
#   ylab('AQI3 days') +
#   xlab('Site')+
#   scale_fill_manual('Legend',
#                     values=c('AP'="red",'ML1'="green",'ML2'="blue", 'OBS'='grey'),
#                     breaks=c('AP','ML1','ML2','OBS')) +
#   theme(legend.position = 'right',legend.title=element_text(size=12),
#         axis.text=element_text(size=15),
#         axis.text.x=element_text(angle = 45, size=10, hjust=1),
#         axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=12),
#         plot.title = element_text(hjust = 0.5,size=20,face="bold"),
#         strip.text.x = element_text(size=15),
#         strip.background = element_rect(colour=NA, fill=NA))+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# 
# jpeg("/bigdata/casus/atmos/ML_model/multi_AQI3_right_O3.jpeg", units="in", width=6, height=5, res=100)
# print(a)
# dev.off()
# 
# #line wrong----
# hss_kss_ap_short <- hss_kss_ap[(hss_kss_ap$OBS_AQI3_days>0),c('OBS_AQI3_days','AP_AQI3_days_wrong','Sitename')]
# hss_kss_ml1_short <- hss_kss_ml1[(hss_kss_ml1$OBS_AQI3_days>0),c('OBS_AQI3_days','ML1_AQI3_days_wrong','Sitename')]
# hss_kss_ml2_short <- hss_kss_ml2[(hss_kss_ml2$OBS_AQI3_days>0),c('OBS_AQI3_days','ML2_AQI3_days_wrong','Sitename')]
# 
# library(reshape2)
# temp1 <- merge(hss_kss_ap_short,hss_kss_ml1_short)
# temp2 <- merge(temp1,hss_kss_ml2_short)
# colnames(temp2) <- c("OBS","Sitename","AP","ML1","ML2")
# #temp2 <- temp2[order(temp2$OBS),]
# temp2[temp2$AP>0,'AP'] <- temp2[temp2$AP>0,'AP']-0.1
# temp2[temp2$ML2>0,'ML2'] <- temp2[temp2$ML2>0,'ML2']+0.1
# 
# #temp2$Sitename <- factor(temp2$Sitename, levels = temp2$Sitename)
# a<-ggplot(data=melt(temp2),aes(x=Sitename, y=value, group=variable, fill=variable))+
#   geom_bar(stat="identity", position=position_dodge())+
#   ylab('AQI3 days') +
#   xlab('Site')+
#   scale_fill_manual('Legend',
#                     values=c('AP'="red",'ML1'="green",'ML2'="blue", 'OBS'='grey'),
#                     breaks=c('AP','ML1','ML2','OBS')) +
#   theme(legend.position = 'right',legend.title=element_text(size=12),
#         axis.text=element_text(size=15),
#         axis.text.x=element_text(angle = 45, size=10, hjust=1),
#         axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=12),
#         plot.title = element_text(hjust = 0.5,size=20,face="bold"),
#         strip.text.x = element_text(size=15),
#         strip.background = element_rect(colour=NA, fill=NA))+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# 
# jpeg("/bigdata/casus/atmos/ML_model/multi_AQI3_wrong_O3.jpeg", units="in", width=6, height=5, res=100)
# print(a)
# dev.off()

#hss----
hss_kss_ap_short <- hss_kss_ap[,c('AP_hss','Sitename')]
hss_kss_ml1_short <- hss_kss_ml1[,c('ML1_hss','Sitename')]
hss_kss_ml2_short <- hss_kss_ml2[,c('ML2_hss','Sitename')]
hss_kss_Combined_short <- hss_kss_Combined[,c('Combined_hss','Sitename')]

temp1 <- merge(hss_kss_ap_short,hss_kss_ml1_short)
temp2 <- merge(temp1,hss_kss_ml2_short)
temp3 <- merge(temp2, hss_kss_Combined_short)

colnames(temp3) <- c("Sitename","AP","ML1","ML2","Combined")
#temp2 <- temp2[complete.cases(temp2),]
temp3 <- temp3[order(temp3$AP),]

temp3$Sitename <- factor(temp3$Sitename, levels = temp3$Sitename)

# a<-ggplot(data=melt(temp2),aes(x=Sitename, y=value, group=variable, color=variable))+
#   geom_line()+
#   ylab('HSS') +
#   xlab('Site')+
#   scale_color_manual('Legend',
#                      values=c('AP'="red",'ML1'="green",'ML2'="blue", 'OBS'='grey'),
#                      breaks=c('AP','ML1','ML2','OBS')) +
#   theme(legend.position = 'right',legend.title=element_text(size=12),
#         axis.text=element_text(size=15),
#         axis.text.x= element_blank(), #element_text(angle = 45, size=10,hjust = 1),
#         axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=12),
#         plot.title = element_text(hjust = 0.5,size=20,face="bold"),
#         strip.text.x = element_text(size=15),
#         strip.background = element_rect(colour=NA, fill=NA))+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# 
# jpeg("/bigdata/casus/atmos/ML_model/multi_HSS_O3.jpeg", units="in", width=8, height=3, res=100)
# print(a)
# dev.off()

a<-ggplot()+
  geom_boxplot(data=melt(temp3), aes_string(x='variable', y='value', group = 'variable'),outlier.shape=NA)+
  ylab('HSS') +
  xlab('')+
  theme(legend.position = 'right',legend.title=element_text(size=12),
        axis.text=element_text(size=15),
        axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=12),
        plot.title = element_text(hjust = 0.5,size=20,face="bold"),
        strip.text.x = element_text(size=15),
        strip.background = element_rect(colour=NA, fill=NA))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1))

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_HSS_box_O3.jpeg", units="in", width=5, height=5, res=100)
print(a)
dev.off()


#kss----
hss_kss_ap_short <- hss_kss_ap[,c('AP_kss','Sitename')]
hss_kss_ml1_short <- hss_kss_ml1[,c('ML1_kss','Sitename')]
hss_kss_ml2_short <- hss_kss_ml2[,c('ML2_kss','Sitename')]
hss_kss_Combined_short <- hss_kss_Combined[,c('Combined_kss','Sitename')]

temp1 <- merge(hss_kss_ap_short,hss_kss_ml1_short)
temp2 <- merge(temp1,hss_kss_ml2_short)
temp3 <- merge(temp2,hss_kss_Combined_short)

colnames(temp3) <- c("Sitename","AP","ML1","ML2","Combined")
#temp2 <- temp2[complete.cases(temp2),]
temp3 <- temp3[order(temp3$AP),]

# temp2$Sitename <- factor(temp2$Sitename, levels = temp2$Sitename)
# a<-ggplot(data=melt(temp2),aes(x=Sitename, y=value, group=variable, color=variable))+
#   geom_line()+
#   ylab('KSS') +
#   xlab('Site')+
#   scale_color_manual('Legend',
#                      values=c('AP'="red",'ML1'="green",'ML2'="blue", 'OBS'='grey'),
#                      breaks=c('AP','ML1','ML2','OBS')) +
#   theme(legend.position = 'right',legend.title=element_text(size=12),
#         axis.text=element_text(size=15),
#         axis.text.x=element_blank(), #element_text(angle = 45, size=10,hjust = 1),
#         axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=12),
#         plot.title = element_text(hjust = 0.5,size=20,face="bold"),
#         strip.text.x = element_text(size=15),
#         strip.background = element_rect(colour=NA, fill=NA))+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# 
# jpeg("/bigdata/casus/atmos/ML_model/multi_KSS_O3.jpeg", units="in", width=8, height=3, res=100)
# print(a)
# dev.off()

a<-ggplot()+
  geom_boxplot(data=melt(temp3), aes_string(x='variable', y='value', group = 'variable'),outlier.shape=NA)+
  ylab('KSS') +
  xlab('')+
  theme(legend.position = 'right',legend.title=element_text(size=12),
        axis.text=element_text(size=15),
        axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=12),
        plot.title = element_text(hjust = 0.5,size=20,face="bold"),
        strip.text.x = element_text(size=15),
        strip.background = element_rect(colour=NA, fill=NA))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1))

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_KSS_box_O3.jpeg", units="in", width=5, height=5, res=100)
print(a)
dev.off()

#soccer plot----
hss_kss_ap_short <- hss_kss_ap[,c('AP_hss','AP_kss','AP_ts1','AP_ts2','AP_ts3','AP_ts4',
                                  'OBS_mean','AP_NMB','AP_NMB1','AP_NMB2','AP_NMB3','AP_NMB4','Sitename')]
hss_kss_ml1_short <- hss_kss_ml1[,c('ML1_hss','ML1_kss','ML1_ts1','ML1_ts2','ML1_ts3','ML1_ts4',
                                    'ML1_NMB','ML1_NMB1','ML1_NMB2','ML1_NMB3','ML1_NMB4','Sitename')]
hss_kss_ml2_short <- hss_kss_ml2[,c('ML2_hss','ML2_kss','ML2_ts1','ML2_ts2','ML2_ts3','ML2_ts4',
                                    'ML2_NMB','ML2_NMB1','ML2_NMB2','ML2_NMB3','ML2_NMB4','Sitename')]
hss_kss_Combined_short <- hss_kss_Combined[,c('Combined_hss','Combined_kss','Combined_ts1','Combined_ts2','Combined_ts3','Combined_ts4',
                                    'Combined_NMB','Combined_NMB1','Combined_NMB2','Combined_NMB3','Combined_NMB4','Sitename')]

library(reshape2)
temp1 <- merge(hss_kss_ap_short,hss_kss_ml1_short)
temp2 <- merge(temp1,hss_kss_ml2_short)
temp3 <- merge(temp2,hss_kss_Combined_short)
# temp <- melt(temp2)
#temp2 <- temp2[complete.cases(temp2),]

models = c('AP','ML1','ML2',"Combined")

for (m in 1:4){
  jpeg(paste("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_HSS_soccer_",models[m],".jpeg",sep=''), units="in", width=5, height=5, res=100)
  xlabel <- "NMB (%)"					# Set x-axis label as NMB
  ylabel <- "HSS"	
  par(mai=c(1,1,1,1.5))#,mfrow=c(1,3))									# Set margins
  
  plot(1, 1, type="n",pch=2, col="green", ylim=c(-0.2,1), xlim=c(-30, 30), xlab=xlabel, ylab=ylabel, cex.axis=2, cex.lab=2, axes=FALSE)
  axis(1,at=c(-30,-15,0,15,30),cex.axis=2, cex.lab=2) 		# Create x-axis
  axis(2,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2, las=2, line=-1)					# Create left side y-axis
  axis(4,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2,las=2)					# Create right side y-axis
  mtext(ylabel,side=4,adj=0.5,line=5,cex=2)
  lines(c(0,0),c(-0.2,1))									# Draw center line
  lines(c(-10,-10),c(-0.2,0.3),lty=2)
  lines(c(-30,-30),c(-0.2,0.6),lty=2)
  lines(c(10,10),c(-0.2,0.3),lty=2)
  lines(c(30,30),c(-0.2,0.6),lty=2)
  lines(c(-10,10),c(0.3,0.3),lty=2)
  lines(c(-30,30),c(0.6,0.6),lty=2)
  abline(h=1) 										# Create top border line
  abline(h=0)
  
  rbPal <- colorRampPalette(c('blue','white','red'))
  temp3$Col <- rbPal(10)[as.numeric(cut(temp3$OBS_mean,breaks = seq(30, 60, by = 3)))]
  for (i in 1:nrow(temp3)) {							# Plot each available species
    points(temp3[i,paste(models[m],'_NMB',sep='')],temp3[i,paste(models[m],'_hss',sep='')],col = temp3$Col[i],pch=16,cex=2)
  } 
  dev.off()
}

library(grDevices)
jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_soccer_legend_MDA8.jpeg", units="in", width=2, height=5, res=100)
legend_image <- as.raster(matrix(rev(rbPal(10)), ncol=1))
plot(c(0,2),c(30,60),type = 'n', axes = F,xlab = '', ylab = '', main = 'MDA8_O3 (ppb)')
text(x=1.5, y = seq(30, 60, by = 10), labels = seq(30, 60, by = 10))
rasterImage(legend_image, 0, 30, 1,60)
dev.off()

for (m in 1:4){
  jpeg(paste("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_KSS_soccer_",models[m],".jpeg",sep=''), units="in", width=5, height=5, res=100)
  xlabel <- "NMB (%)"					# Set x-axis label as NMB
  ylabel <- "KSS"	
  par(mai=c(1,1,1,1.5)) #,mfrow=c(1,3))									# Set margins
  
  plot(1, 1, type="n",pch=2, col="green", ylim=c(-0.2,1), xlim=c(-30, 30), xlab=xlabel, ylab=ylabel, cex.axis=2, cex.lab=2, axes=FALSE)
  axis(1,at=c(-30,-15,0,15,30),cex.axis=2, cex.lab=2) 		# Create x-axis
  axis(2,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2, las=2, line=-1)					# Create left side y-axis
  axis(4,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2,las=2)					# Create right side y-axis
  mtext(ylabel,side=4,adj=0.5,line=5,cex=2)
  lines(c(0,0),c(-0.2,1))									# Draw center line
  lines(c(-10,-10),c(-0.2,0.3),lty=2)
  lines(c(-30,-30),c(-0.2,0.6),lty=2)
  lines(c(10,10),c(-0.2,0.3),lty=2)
  lines(c(30,30),c(-0.2,0.6),lty=2)
  lines(c(-10,10),c(0.3,0.3),lty=2)
  lines(c(-30,30),c(0.6,0.6),lty=2)
  abline(h=1) 										# Create top border line
  abline(h=0)
  
  rbPal <- colorRampPalette(c('blue','white','red'))
  temp3$Col <- rbPal(10)[as.numeric(cut(temp3$OBS_mean,breaks = seq(30, 60, by = 3)))]
  for (i in 1:nrow(temp3)) {							# Plot each available species
    points(temp3[i,paste(models[m],'_NMB',sep='')],temp3[i,paste(models[m],'_kss',sep='')],col = temp3$Col[i],pch=16,cex=2)
  } 
  dev.off()
}

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_TS_soccer_O3.jpeg", units="in", width=20, height=20, res=100)
xlabel <- "NMB (%)"					# Set x-axis label as NMB
#graphics.off()
#par("mar")
par(mai=c(0.5,1,0.5,1),mfrow=c(4,4))# Set margins
ts_label=c('CSI1','CSI2','CSI3','CSI4')
ts_list=c('ts1','ts2','ts3','ts4')
for (t in 1:4){
  ylabel <- ts_label[t]
  #plot(1, 1, type="n",pch=2, col="green", ylim=c(-0.2,1), xlim=c(-100, 200), xlab=xlabel, ylab=ylabel, cex.axis=2, cex.lab=2, axes=FALSE)
  #axis(1,at=c(-100,-75,-60,-45,-30,-15,0,15,30,45,60,75,100,12,5150,175,200),cex.axis=2, cex.lab=2) 		# Create x-axis
  plot(1, 1, type="n",pch=2, col="green", ylim=c(-0.2,1), xlim=c(-30, 30), xlab=xlabel, ylab=ylabel, cex.axis=2, cex.lab=2, axes=FALSE)
  axis(1,at=c(-30,-15,0,15,30),cex.axis=2,cex.lab=2) 		# Create x-axis
  axis(2,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2, las=2, line=-1)					# Create left side y-axis
  axis(4,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2,las=2)					# Create right side y-axis
  mtext(ylabel,side=4,adj=0.5,line=5,cex=1.4)
  lines(c(0,0),c(-0.2,1))									# Draw center line
  lines(c(-10,-10),c(-0.2,0.3),lty=2)
  lines(c(-30,-30),c(-0.2,0.6),lty=2)
  lines(c(10,10),c(-0.2,0.3),lty=2)
  lines(c(30,30),c(-0.2,0.6),lty=2)
  lines(c(-10,10),c(0.3,0.3),lty=2)
  lines(c(-30,30),c(0.6,0.6),lty=2)
  abline(h=1) 										# Create top border line
  abline(h=0)
  
  rbPal <- colorRampPalette(c('blue','white','red'))
  temp3$Col <- rbPal(10)[as.numeric(cut(temp3$OBS_mean,breaks = seq(30, 60, by = 3)))]
  for (i in 1:nrow(temp3)) {							# Plot each available species
    if (complete.cases(temp3[i,c(paste('AP_NMB',t,sep=''),paste('AP_',ts_list[t],sep=''))]) == TRUE) {
      points(temp3[i,paste('AP_NMB',t,sep='')],temp3[i,paste('AP_',ts_list[t],sep='')],col = temp3$Col[i],pch=16,cex=2)
    }
  } 
}

for (t in 1:4){
  ylabel <- ts_label[t]
  #par(mai=c(1,1,0.5,1))									# Set margins
  plot(1, 1, type="n",pch=2, col="green", ylim=c(-0.2,1), xlim=c(-30, 30), xlab=xlabel, ylab=ylabel, cex.axis=2, cex.lab=2, axes=FALSE)
  axis(1,at=c(-30,-15,0,15,30),cex.axis=2,cex.lab=2) 		# Create x-axis
  axis(2,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2,las=2,cex.lab=2,line = -1)					# Create left side y-axis
  axis(4,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2,las=2)					# Create right side y-axis
  mtext(ylabel,side=4,adj=0.5,line=5,cex=1.4)
  lines(c(0,0),c(-0.2,1))									# Draw center line
  lines(c(-10,-10),c(-0.2,0.3),lty=2)
  lines(c(-30,-30),c(-0.2,0.6),lty=2)
  lines(c(10,10),c(-0.2,0.3),lty=2)
  lines(c(30,30),c(-0.2,0.6),lty=2)
  lines(c(-10,10),c(0.3,0.3),lty=2)
  lines(c(-30,30),c(0.6,0.6),lty=2)
  abline(h=1) 										# Create top border line
  abline(h=0)
  
  rbPal <- colorRampPalette(c('blue','white','red'))
  temp3$Col <- rbPal(10)[as.numeric(cut(temp3$OBS_mean,breaks = seq(30, 60, by = 3)))]
  for (i in 1:nrow(temp3)) {							# Plot each available species
    if (complete.cases(temp3[i,c(paste('ML1_NMB',t,sep=''),paste('ML1_',ts_list[t],sep=''))]) == TRUE) {
      points(temp3[i,paste('ML1_NMB',t,sep='')],temp3[i,paste('ML1_',ts_list[t],sep='')],col = temp3$Col[i],pch=16,cex=2)
    }
  } 
}

for (t in 1:4){
  ylabel <- ts_label[t]
  #par(mai=c(1,1,0.5,1))									# Set margins
  plot(1, 1, type="n",pch=2, col="green", ylim=c(-0.2,1), xlim=c(-30, 30), xlab=xlabel, ylab=ylabel, cex.axis=2, cex.lab=2, axes=FALSE)
  axis(1,at=c(-30,-15,0,15,30),cex.axis=2,cex.lab=2) 		# Create x-axis
  axis(2,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2,las=2,line=-1)					# Create left side y-axis
  axis(4,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2,las=2)					# Create right side y-axis
  mtext(ylabel,side=4,adj=0.5,line=5,cex=1.4)
  lines(c(0,0),c(-0.2,1))									# Draw center line
  lines(c(-10,-10),c(-0.2,0.3),lty=2)
  lines(c(-30,-30),c(-0.2,0.6),lty=2)
  lines(c(10,10),c(-0.2,0.3),lty=2)
  lines(c(30,30),c(-0.2,0.6),lty=2)
  lines(c(-10,10),c(0.3,0.3),lty=2)
  lines(c(-30,30),c(0.6,0.6),lty=2)
  abline(h=1) 										# Create top border line
  abline(h=0)
  
  rbPal <- colorRampPalette(c('blue','white','red'))
  temp3$Col <- rbPal(10)[as.numeric(cut(temp3$OBS_mean,breaks = seq(30, 60, by = 3)))]
  for (i in 1:nrow(temp3)) {							# Plot each available species
    if (complete.cases(temp3[i,c(paste('ML2_NMB',t,sep=''),paste('ML2_',ts_list[t],sep=''))]) == TRUE) {
      points(temp3[i,paste('ML2_NMB',t,sep='')],temp3[i,paste('ML2_',ts_list[t],sep='')],col = temp3$Col[i],pch=16,cex=2)
    }
  } 
}

for (t in 1:4){
  ylabel <- ts_label[t]
  #par(mai=c(1,1,0.5,1))									# Set margins
  plot(1, 1, type="n",pch=2, col="green", ylim=c(-0.2,1), xlim=c(-30, 30), xlab=xlabel, ylab=ylabel, cex.axis=2, cex.lab=2, axes=FALSE)
  axis(1,at=c(-30,-15,0,15,30),cex.axis=2,cex.lab=2) 		# Create x-axis
  axis(2,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2,las=2,line=-1)					# Create left side y-axis
  axis(4,at=c(-0.2,0,0.2,0.4,0.6,0.8,1),cex.axis=2,las=2)					# Create right side y-axis
  mtext(ylabel,side=4,adj=0.5,line=5,cex=1.4)
  lines(c(0,0),c(-0.2,1))									# Draw center line
  lines(c(-10,-10),c(-0.2,0.3),lty=2)
  lines(c(-30,-30),c(-0.2,0.6),lty=2)
  lines(c(10,10),c(-0.2,0.3),lty=2)
  lines(c(30,30),c(-0.2,0.6),lty=2)
  lines(c(-10,10),c(0.3,0.3),lty=2)
  lines(c(-30,30),c(0.6,0.6),lty=2)
  abline(h=1) 										# Create top border line
  abline(h=0)
  
  rbPal <- colorRampPalette(c('blue','white','red'))
  temp3$Col <- rbPal(10)[as.numeric(cut(temp3$OBS_mean,breaks = seq(30, 60, by = 3)))]
  for (i in 1:nrow(temp3)) {							# Plot each available species
    points(temp3[i,paste('Combined_NMB',t,sep='')],temp3[i,paste('Combined_',ts_list[t],sep='')],col = temp3$Col[i],pch=16,cex=2)
  } 
}

dev.off()

#map----
library(ggmap)
library(scales)

site_info <- read.csv('/Users/fankai/OneDrive - Washington State University (email.wsu.edu)/ML/Unique_monitoring_sites_and_locations_for_all_pollutants.csv', stringsAsFactors=FALSE)
site_info$AQS_ID <- formatC(as.numeric(site_info$AQS_ID), width = 9, format = "d", flag = "0")
temp3<-merge(temp3,site_info)

write.csv(temp3,"/bigdata/casus/atmos/ML_model/multi_o3_cv/stat.csv")

test <- get_map(location = c(-130,40,-105,50), maptype = 'terrain')
jet.colors0 <- colorRampPalette(c("#00007F", "blue", "#007FFF", "cyan", "white", "yellow", "#FF7F00", "red", "#7F0000"))

a<-ggmap(test) +
  coord_map()+
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = AP_hss #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("HSS",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=seq(0, 1, by=0.2),limits=c(0,1), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23, #
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_HSS_map_AP.jpeg", units="in", width=5, height=5, res=100)
print(a)
dev.off()

b<-ggmap(test) +
  coord_map()+
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = ML1_hss #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("HSS",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=seq(0, 1, by=0.2),limits=c(0,1), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23,
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_HSS_map_ML1.jpeg", units="in", width=5, height=5, res=100)
print(b)
dev.off()

c<-ggmap(test) +
  coord_map()+
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = ML2_hss #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("HSS",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=seq(0, 1, by=0.2),limits=c(0,1), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23,
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_HSS_map_ML2.jpeg", units="in", width=5, height=5, res=100)
print(c)
dev.off()

d<-ggmap(test) +
  coord_map()+
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = Combined_hss #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("HSS",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=seq(0, 1, by=0.2),limits=c(0,1), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23,
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_HSS_map_Combined.jpeg", units="in", width=5, height=5, res=100)
print(d)
dev.off()
# jpeg("/bigdata/casus/atmos/ML_model/multi_HSS_map_O3.jpeg", units="in", width=15, height=5, res=100)
# library(ggpubr)
# a<-ggarrange(a, b, c, 
#              #labels = c("ML1", "ML2", "AP"),
#              ncol = 3, nrow = 1)
# print(a)
# dev.off()

a<-ggmap(test) +
  coord_map()+
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = AP_kss #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("KSS",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=seq(0, 1, by=0.2),limits=c(0,1), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23, #
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_KSS_map_AP.jpeg", units="in", width=5, height=5, res=100)
print(a)
dev.off()

b<-ggmap(test) +
  coord_map()+
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = ML1_kss #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("KSS",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=seq(0, 1, by=0.2),limits=c(0,1), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23,
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_KSS_map_ML1.jpeg", units="in", width=5, height=5, res=100)
print(b)
dev.off()

c<-ggmap(test) +
  coord_map()+
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = ML2_kss #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("KSS",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=seq(0, 1, by=0.2),limits=c(0,1), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23,
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_KSS_map_ML2.jpeg", units="in", width=5, height=5, res=100)
print(c)
dev.off()

d<-ggmap(test) +
  coord_map()+
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = Combined_kss #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("KSS",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=seq(0, 1, by=0.2),limits=c(0,1), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23,
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_KSS_map_Combined.jpeg", units="in", width=5, height=5, res=100)
print(d)
dev.off()

# jpeg("/bigdata/casus/atmos/ML_model/multi_KSS_map_O3.jpeg", units="in", width=15, height=5, res=100)
# library(ggpubr)
# a<-ggarrange(a, b, c, 
#              #labels = c("ML1", "ML2", "AP"),
#              ncol = 3, nrow = 1)
# print(a)
# dev.off()

#map nmb----
a<-ggmap(test) +
  #geom_path(data=states,aes(x=long,y=lat,group=group),colour="black",lty = 1, lwd = 0.3)+  
  #geom_path(data=counties,aes(x=long,y=lat,group=group),colour="black",lty = 1, lwd = 0.5)+  
  #borders("state", regions = CA, colour="black", lty = 1, lwd = 1) +
  coord_map()+
  #ggtitle("O3") + 
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = AP_NMB #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("NMB, %",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=ceiling(seq(-30, 30, by=10)),limits=c(-30,30), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23,#
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",
        legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_NMB_map_AP.jpeg", units="in", width=5, height=5, res=100)
print(a)
dev.off()

b<-ggmap(test) +
  #geom_path(data=states,aes(x=long,y=lat,group=group),colour="black",lty = 1, lwd = 0.3)+  
  #geom_path(data=counties,aes(x=long,y=lat,group=group),colour="black",lty = 1, lwd = 0.5)+  
  #borders("state", regions = CA, colour="black", lty = 1, lwd = 1) +
  coord_map()+
  #ggtitle("O3") + 
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = ML1_NMB #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8_O3 Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("NMB, %",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=ceiling(seq(-30, 30, by=5)),limits=c(-30,30), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 20, #barwidth = 2, barheight = 23,
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_NMB_map_ML1.jpeg", units="in", width=5, height=5, res=100)
print(b)
dev.off()

c<-ggmap(test) +
  #geom_path(data=states,aes(x=long,y=lat,group=group),colour="black",lty = 1, lwd = 0.3)+  
  #geom_path(data=counties,aes(x=long,y=lat,group=group),colour="black",lty = 1, lwd = 0.5)+  
  #borders("state", regions = CA, colour="black", lty = 1, lwd = 1) +
  coord_map()+
  #ggtitle("O3") + 
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = ML2_NMB #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8 Obs. Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("NMB, %",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=ceiling(seq(-30, 30, by=5)),limits=c(-30,30), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 2, barheight = 23,
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_NMB_map_ML2.jpeg", units="in", width=5, height=5, res=100)
print(c)
dev.off()

d<-ggmap(test) +
  #geom_path(data=states,aes(x=long,y=lat,group=group),colour="black",lty = 1, lwd = 0.3)+  
  #geom_path(data=counties,aes(x=long,y=lat,group=group),colour="black",lty = 1, lwd = 0.5)+  
  #borders("state", regions = CA, colour="black", lty = 1, lwd = 1) +
  coord_map()+
  #ggtitle("O3") + 
  # for values at site locations
  geom_point(data = temp3,aes(x=Longitude, y =Latitude, size = OBS_mean, fill = Combined_NMB #, 
                              #shape=location_setting
  ), alpha=0.7, shape=21)+
  #for O3
  scale_size_continuous("MDA8 Obs. Mean\n  (ppb)",
                        #limits=c(0,15),breaks=c(1,2,5,10),range = c(1,15)
                        range = c(4,12)
  )+
  #NMBs
  # using jet colors
  scale_fill_gradientn("NMB, %",colours = jet.colors0(7), #c("#00007F", "#002AFF", "#00D4FF", "#FFFFFF"), #
                       breaks=ceiling(seq(-30, 30, by=5)),limits=c(-30,30), oob=squish) +
  #breaks=ceiling(seq(-100, -30, by=20)),limits=c(-100,-30), oob=squish) +
  #scale_shape_manual("Location Setting",values = c(21, 22, 24))+
  # ordering what legent you want to show first and second
  guides( size = guide_legend(order = 1, size =7),
          fill = guide_colourbar(order = 2, barwidth = 2, barheight = 23,
                                 raster = FALSE, ticks = TRUE,
                                 draw.ulim = TRUE, draw.llim = TRUE),
          shape = guide_legend(order = 3,override.aes = list(size=4))) +
  #scale_y_continuous(limits=c(42.2, 48.7)) +
  #scale_x_continuous(limits = c(-125, -115)) +
  #scale_y_continuous(limits = c(32, 43)) +
  #scale_x_continuous(limits = c(-119, -117)) +
  #scale_y_continuous(limits = c(33, 35)) +
  #scale_x_continuous(limits = c(-123, -119)) +
  #scale_y_continuous(limits = c(35, 39)) +
  theme_bw() +
  #without labels in axis and texts in tickmarks 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank()) +
  theme(panel.background = element_rect(colour = "black", size = 1.5)) +
  # outline around legent boxes
  theme(legend.position = "none",legend.spacing  = unit(0.5, "cm"),
        legend.box = "vertical") +
  theme(plot.title = element_text(size = 15, lineheight=.8, face="bold"),
        legend.title=element_text(size = 14,face="bold"),
        legend.text=element_text(face="bold",size = 14)) 

jpeg("/bigdata/casus/atmos/ML_model/multi_o3_cv/multi_NMB_map_Combined.jpeg", units="in", width=5, height=5, res=100)
print(d)
dev.off()
# jpeg("/bigdata/casus/atmos/ML_model/multi_map_nmb_O3.jpeg", units="in", width=15, height=5, res=100)
# library(ggpubr)
# a<-ggarrange(a, b, c, 
#              #labels = c("ML1", "ML2", "AP"),
#              ncol = 3, nrow = 1)
# print(a)
# dev.off()

# #qq plot----
# df <- merge(o3_RFc[,c(1,7:9,14)],o3_2RF[,c(1,9,15)],
#             by=c('datetime','site'))
# df <- df[complete.cases(df),]
# df$datetime <- paste(date(as.POSIXlt(df$datetime)),df$site)
# colnames(df) <- c('datetime','site','Obs','ML1','AP','ML2')
# df['Obs'] <- sort(df[['Obs']])
# df['AP'] <- sort(df[['AP']])
# df['ML1'] <- sort(df[['ML1']])
# df['ML2'] <- sort(df[['ML2']])
# subdf <- melt(df[,c(1,3:6)],id.vars = c('datetime','Obs'))
# 
# jpeg("/bigdata/casus/atmos/ML_model/multi_qq_full_O3.jpeg", units="in", width=5, height=5, res=100)
# a<-ggplot(subdf)+
#   geom_point(aes(x=Obs,y=value,color=variable))+
#   geom_abline(intercept = 0, slope = 1, size=1.5)+
#   scale_color_manual('',
#                      values=c('AP'="red",'ML1'="green",'ML2'="blue"),
#                      breaks=c('AP','ML1','ML2')) +
#   xlab('Obs. MDA8 (ppb)')+
#   ylab('Model MDA8 (ppb)')+
#   #xlim(20,80)+
#   #ylim(20,80)+
#   theme(legend.position = c(0.15,0.8),legend.title=element_text(size=15),
#         axis.text=element_text(size=20),
#         axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=20),
#         plot.title = element_text(hjust = 0.5,size=20,face="bold"),
#         strip.text.x = element_text(size=20),
#         strip.background = element_rect(colour=NA, fill=NA))+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# print(a)
# dev.off()
# 
# jpeg("/bigdata/casus/atmos/ML_model/multi_qq_O3.jpeg", units="in", width=5, height=5, res=100)
# a<-ggplot(subdf)+
#   geom_point(aes(x=Obs,y=value,color=variable))+
#   geom_abline(intercept = 0, slope = 1, size=1.5)+
#   scale_color_manual('',
#                      values=c('AP'="red",'ML1'="green",'ML2'="blue"),
#                      breaks=c('AP','ML1','ML2')) +
#   xlab('Obs. MDA8 (ppb)')+
#   ylab('Model MDA8 (ppb)')+
#   xlim(0,90)+
#   ylim(0,90)+
#   theme(legend.position = c(0.15,0.8),legend.title=element_text(size=15),
#         axis.text=element_text(size=20),
#         axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=20),
#         plot.title = element_text(hjust = 0.5,size=20,face="bold"),
#         strip.text.x = element_text(size=20),
#         strip.background = element_rect(colour=NA, fill=NA))+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
#         panel.background = element_blank(), axis.line = element_line(colour = "black"),
#         panel.border = element_rect(colour = "black", fill=NA, size=1))
# print(a)
# dev.off()
# 
# # df <- merge(o3_RFc[,c(1,7:9,14)],o3_2RF[,c(1,9,15)],
# #             by=c('datetime','site'))
# # df <- df[complete.cases(df),]
# # df$datetime <- paste(date(as.POSIXlt(df$datetime)),df$site)
# # colnames(df) <- c('datetime','site','AP','Obs','ML1','ML2')
# # g<-ggplot()+
# #   scale_color_manual('',
# #                      values=c('ML1'="green",'ML2'="blue"),
# #                      breaks=c('ML1','ML2')) +
# #   xlab('Days')+
# #   ylab('NMB (%)')+
# #   theme(legend.position = c(0.8,0.8),legend.title=element_text(size=15),
# #         axis.text=element_text(size=20),
# #         axis.title=element_text(size=20,face="bold"), legend.text=element_text(size=20),
# #         plot.title = element_text(hjust = 0.5,size=20,face="bold"),
# #         strip.text.x = element_text(size=20),
# #         strip.background = element_rect(colour=NA, fill=NA))+
# #   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
# #         panel.background = element_blank(), axis.line = element_line(colour = "black"),
# #         panel.border = element_rect(colour = "black", fill=NA, size=1))
# # for (s in unique(df$site)){
# #   subdf <- df[df$site == s,]
# #   ML_nmb <- data.frame(matrix(NA,nrow=nrow(subdf),ncol=5))
# #   colnames(ML_nmb) <- c('Days','Date1','ML1','Date2','ML2')
# #   for (i in 1:nrow(subdf)){
# #     tmp <- subdf[1:i,]
# #     ML_nmb[i,1] <- i
# #     ML_nmb[i,2] <- as.character(subdf$datetime[i])
# #     ML_nmb[i,3] <- round(sum(tmp$ML1 - tmp$Obs,na.rm = T)/
# #                            sum(tmp$Obs,na.rm = T)*100,1)
# #   }
# #   
# #   for (i in 1:nrow(subdf)){
# #     tmp <- subdf[1:i,]
# #     ML_nmb[i,4] <- as.character(subdf$datetime[i])
# #     ML_nmb[i,5] <- round(sum(tmp$ML2 - tmp$Obs,na.rm = T)/
# #                            sum(tmp$Obs,na.rm = T)*100,1)
# #   }
# #   
# #   ML_nmb <- ML_nmb[,c('Days','ML1','ML2')]
# #   
# #   g <- g + 
# #     geom_point(data=melt(ML_nmb,id.vars = 'Days'),
# #                aes(x=Days,y=value,col=variable),alpha=0.3)
# # }
# # jpeg("/bigdata/casus/atmos/ML_model/multi_nmb_change_O3.jpeg", units="in", width=5, height=5, res=100)
# # print(g)
# # dev.off()
# 
# df <- merge(o3_RFc[,c(1,10:12,14)],o3_2RF[,c(1,12,15)],
#             by=c('datetime','site'))
# df <- df[complete.cases(df),]
# colnames(df) <- c('datetime','site','Obs','ML1','AP','ML2')
# nrow(df[df$Obs>2&df$AP>2,])
# nrow(df[df$Obs>2&df$AP<3,])
# nrow(df[df$Obs<3&df$AP>2,])
# nrow(df[df$Obs<3&df$AP<3,])
# 
# nrow(df[df$Obs>2&df$ML1>2,])
# nrow(df[df$Obs>2&df$ML1<3,])
# nrow(df[df$Obs<3&df$ML1>2,])
# nrow(df[df$Obs<3&df$ML1<3,])
# 
# nrow(df[df$Obs>2&df$ML2>2,])
# nrow(df[df$Obs>2&df$ML2<3,])
# nrow(df[df$Obs<3&df$ML2>2,])
# nrow(df[df$Obs<3&df$ML2<3,])


#ml1----
df <- merge(o3_RFc,o3_2RF[,c(1,4,6,8)],
            by=c('datetime','site'))
df <- na.omit(df)
colnames(df) <- c('datetime','site','Obs','AP','ML1','AQI_day','ML1_AQI','AP_AQI','ML2','ML2_AQI')
df$Combined <- df$ML2
df$Combined_AQI <- df$ML2_AQI
df[df$ML1>70,'Combined'] <- df[df$ML1>70,'ML1']
df[df$ML1>70,'Combined_AQI'] <- df[df$ML1>70,'ML1_AQI']

#site_info <- read.csv('/Users/fankai/OneDrive - Washington State University (email.wsu.edu)/ML/Unique_monitoring_sites_and_locations_for_all_pollutants.csv', stringsAsFactors=FALSE)
#site_info$AQS_ID <- formatC(as.numeric(site_info$AQS_ID), width = 9, format = "d", flag = "0")
#df <- aggregate(. ~ datetime + site, data = df, mean, na.rm = TRUE)
df$datetime <- as.POSIXct(df$datetime)
sites <- unique(df$site)
stat_table <- data.frame(matrix(NA,nrow=length(unique(df$aqsid))*3,ncol=4))
colnames(stat_table) <- c('obs_1','obs_2','obs_3','obs_4')
hss_kss <- data.frame(matrix(NA,nrow=length(unique(df$aqsid)),ncol=15))
colnames(hss_kss) <- c('OBS_mean','NMB','NME','hss','kss','ts1','ts2','ts3','ts4',
                       'OBS_AQI3_days','AQI3_days_right','AQI3_days_wrong',
                       'OBS_AQI4_days','AQI4_days_right','AQI4_days_wrong')

for (i in 1:4){
    stat_table[i,'obs_1'] <- nrow(df[df$AQI_day==1&df$ML1_AQI==i,])
    stat_table[i,'obs_2'] <- nrow(df[df$AQI_day==2&df$ML1_AQI==i,])
    stat_table[i,'obs_3'] <- nrow(df[df$AQI_day==3&df$ML1_AQI==i,])
    stat_table[i,'obs_4'] <- nrow(df[df$AQI_day==4&df$ML1_AQI==i,])
}
  
  aqi_table = stat_table
  hss_kss[2,'hss'] <- multi.cont(as.matrix(stat_table))$hss
  hss_kss[2,'kss'] <- multi.cont(as.matrix(stat_table))$pss
  hss_kss[2,'ts1'] <- multi.cont(as.matrix(stat_table))$ts[1]
  hss_kss[2,'ts2'] <- multi.cont(as.matrix(stat_table))$ts[2]
  hss_kss[2,'ts3'] <- multi.cont(as.matrix(stat_table))$ts[3]
  hss_kss[2,'ts4'] <- multi.cont(as.matrix(stat_table))$ts[4]
  hss_kss[2,'OBS_AQI3_days'] <- nrow(df[df$AQI_day==3,])
  hss_kss[2,'AQI3_days_right'] <- nrow(df[df$AQI_day==3&df$ML1_AQI==3,])
  hss_kss[2,'AQI3_days_wrong'] <- nrow(df[df$AQI_day!=3&df$ML1_AQI==3,])
  hss_kss[2,'OBS_AQI4_days'] <- nrow(df[df$AQI_day==4,])
  hss_kss[2,'AQI4_days_right'] <- nrow(df[df$AQI_day==4&df$ML1_AQI==4,])
  hss_kss[2,'AQI4_days_wrong'] <- nrow(df[df$AQI_day!=4&df$ML1_AQI==4,])
  hss_kss[2,'OBS_mean'] <- mean(df$Obs)
  hss_kss[2,'NMB'] <- sum(df$ML1 - df$Obs,na.rm = T)/
    sum(df$Obs,na.rm = T)*100
  hss_kss[2,'NME'] <- sum(abs(df$ML1 - df$Obs),na.rm = T)/
    sum(df$Obs,na.rm = T)*100

#airpact----
stat_table <- data.frame(matrix(NA,nrow=4,ncol=4))
colnames(stat_table) <- c('obs_1','obs_2','obs_3','obs_4')
for (i in 1:4){
    stat_table[i,'obs_1'] <- nrow(df[df$AQI_day==1&df$AP_AQI==i,])
    stat_table[i,'obs_2'] <- nrow(df[df$AQI_day==2&df$AP_AQI==i,])
    stat_table[i,'obs_3'] <- nrow(df[df$AQI_day==3&df$AP_AQI==i,])
    stat_table[i,'obs_4'] <- nrow(df[df$AQI_day==4&df$AP_AQI==i,])
  }
  
  aqi_table = stat_table
  hss_kss[1,'hss'] <- multi.cont(as.matrix(aqi_table))$hss
  hss_kss[1,'kss'] <- multi.cont(as.matrix(aqi_table))$pss
  hss_kss[1,'ts1'] <- multi.cont(as.matrix(aqi_table))$ts[1]
  hss_kss[1,'ts2'] <- multi.cont(as.matrix(aqi_table))$ts[2]
  hss_kss[1,'ts3'] <- multi.cont(as.matrix(aqi_table))$ts[3]
  hss_kss[1,'ts4'] <- multi.cont(as.matrix(aqi_table))$ts[4]
  hss_kss[1,'OBS_AQI3_days'] <- nrow(df[df$AQI_day==3,])
  hss_kss[1,'AQI3_days_right'] <- nrow(df[df$AQI_day==3&df$AP_AQI==3,])
  hss_kss[1,'AQI3_days_wrong'] <- nrow(df[df$AQI_day!=3&df$AP_AQI==3,])
  hss_kss[1,'OBS_AQI4_days'] <- nrow(df[df$AQI_day==4,])
  hss_kss[1,'AQI4_days_right'] <- nrow(df[df$AQI_day==4&df$AP_AQI==4,])
  hss_kss[1,'AQI4_days_wrong'] <- nrow(df[df$AQI_day!=4&df$AP_AQI==4,])
  hss_kss[1,'OBS_mean'] <- mean(df$Obs)
  hss_kss[1,'NMB'] <- sum(df$AP - df$Obs,na.rm = T)/
    sum(df$Obs,na.rm = T)*100
  hss_kss[1,'NME'] <- sum(abs(df$AP - df$Obs),na.rm = T)/
    sum(df$Obs,na.rm = T)*100
  
#ml2----
#site_info <- read.csv('/Users/fankai/OneDrive - Washington State University (email.wsu.edu)/ML/Unique_monitoring_sites_and_locations_for_all_pollutants.csv', stringsAsFactors=FALSE)
#site_info$AQS_ID <- formatC(as.numeric(site_info$AQS_ID), width = 9, format = "d", flag = "0")
#df <- aggregate(. ~ datetime + site, data = o3_2RF, mean, na.rm = TRUE)
#df$datetime <- as.POSIXct(df$datetime)
#df <- df[month(df$datetime)>5&month(df$datetime)<9,]
#sites <- unique(df$site)
stat_table <- data.frame(matrix(NA,nrow=4,ncol=4))
colnames(stat_table) <- c('obs_1','obs_2','obs_3','obs_4')
for (i in 1:4){
    stat_table[i,'obs_1'] <- nrow(df[df$AQI_day==1&df$ML2_AQI==i,])
    stat_table[i,'obs_2'] <- nrow(df[df$AQI_day==2&df$ML2_AQI==i,])
    stat_table[i,'obs_3'] <- nrow(df[df$AQI_day==3&df$ML2_AQI==i,])
    stat_table[i,'obs_4'] <- nrow(df[df$AQI_day==4&df$ML2_AQI==i,])
  }
  
  aqi_table = stat_table
  hss_kss[3,'hss'] <- multi.cont(as.matrix(aqi_table))$hss
  hss_kss[3,'kss'] <- multi.cont(as.matrix(aqi_table))$pss
  hss_kss[3,'ts1'] <- multi.cont(as.matrix(aqi_table))$ts[1]
  hss_kss[3,'ts2'] <- multi.cont(as.matrix(aqi_table))$ts[2]
  hss_kss[3,'ts3'] <- multi.cont(as.matrix(aqi_table))$ts[3]
  hss_kss[3,'ts4'] <- multi.cont(as.matrix(aqi_table))$ts[4]
  hss_kss[3,'OBS_AQI3_days'] <- nrow(df[df$AQI_day==3,])
  hss_kss[3,'AQI3_days_right'] <- nrow(df[df$AQI_day==3&df$ML2_AQI==3,])
  hss_kss[3,'AQI3_days_wrong'] <- nrow(df[df$AQI_day!=3&df$ML2_AQI==3,])
  hss_kss[3,'OBS_AQI4_days'] <- nrow(df[df$AQI_day==4,])
  hss_kss[3,'AQI4_days_right'] <- nrow(df[df$AQI_day==4&df$ML2_AQI==4,])
  hss_kss[3,'AQI4_days_wrong'] <- nrow(df[df$AQI_day!=4&df$ML2_AQI==4,])
  hss_kss[3,'OBS_mean'] <- mean(df$Obs)
  hss_kss[3,'NMB'] <- sum(df$ML2 - df$Obs,na.rm = T)/
    sum(df$Obs,na.rm = T)*100
  hss_kss[3,'NME'] <- sum(abs(df$ML2 - df$Obs),na.rm = T)/
    sum(df$Obs,na.rm = T)*100
  
#combined----
stat_table <- data.frame(matrix(NA,nrow=4,ncol=4))
colnames(stat_table) <- c('obs_1','obs_2','obs_3','obs_4')
for (i in 1:4){
    stat_table[i,'obs_1'] <- nrow(df[df$AQI_day==1&df$Combined_AQI==i,])
    stat_table[i,'obs_2'] <- nrow(df[df$AQI_day==2&df$Combined_AQI==i,])
    stat_table[i,'obs_3'] <- nrow(df[df$AQI_day==3&df$Combined_AQI==i,])
    stat_table[i,'obs_4'] <- nrow(df[df$AQI_day==4&df$Combined_AQI==i,])
  }
  
  aqi_table = stat_table
  hss_kss[4,'hss'] <- multi.cont(as.matrix(aqi_table))$hss
  hss_kss[4,'kss'] <- multi.cont(as.matrix(aqi_table))$pss
  hss_kss[4,'ts1'] <- multi.cont(as.matrix(aqi_table))$ts[1]
  hss_kss[4,'ts2'] <- multi.cont(as.matrix(aqi_table))$ts[2]
  hss_kss[4,'ts3'] <- multi.cont(as.matrix(aqi_table))$ts[3]
  hss_kss[4,'ts4'] <- multi.cont(as.matrix(aqi_table))$ts[4]
  hss_kss[4,'OBS_AQI3_days'] <- nrow(df[df$AQI_day==3,])
  hss_kss[4,'AQI3_days_right'] <- nrow(df[df$AQI_day==3&df$Combined_AQI==3,])
  hss_kss[4,'AQI3_days_wrong'] <- nrow(df[df$AQI_day!=3&df$Combined_AQI==3,])
  hss_kss[4,'OBS_AQI4_days'] <- nrow(df[df$AQI_day==4,])
  hss_kss[4,'AQI4_days_right'] <- nrow(df[df$AQI_day==4&df$Combined_AQI==4,])
  hss_kss[4,'AQI4_days_wrong'] <- nrow(df[df$AQI_day!=4&df$Combined_AQI==4,])
  hss_kss[4,'OBS_mean'] <- mean(df$Obs)
  hss_kss[4,'NMB'] <- sum(df$Combined - df$Obs,na.rm = T)/
    sum(df$Obs,na.rm = T)*100
  hss_kss[4,'NME'] <- sum(abs(df$Combined - df$Obs),na.rm = T)/
    sum(df$Obs,na.rm = T)*100