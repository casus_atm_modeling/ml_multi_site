import pylab
#pandas
import pandas as pd
print('pandas: %s' % pd.__version__)
#datetime
import datetime as dt
#import numpy
import numpy as np
#anchored text
from matplotlib.offsetbox import AnchoredText 
import time
import glob
import pickle
import os

#open functions
exec(open(r"./ML_Functions.py").read())

gmtoff = pd.read_csv('./gmtoff.csv')

with open('./data/WRF_pkl/multi_sites_full_o3_1720.pkl', 'rb') as fp:
    dict_of_dt = pickle.load(fp)

#create the results dir if it doesn't exist
outputdir="./data/WRF_pkl_repl_w_AQS"
os.makedirs(outputdir, exist_ok=True)

for s in list(dict_of_dt.keys()):
    aqsid = s
    print(aqsid)
    
    dfPm = dict_of_dt[s]
    dfPm = dfPm[(dfPm.index.month >4) & (dfPm.index.month < 10)]
    #dfPm = dfPm.drop('O3_pred',1).dropna()
    if(len(dfPm)==0): continue
    
    try: 
        f = open("./data/WRF_pkl/data_wrf2020_"+s+".pkl",'rb')
        try:
            obs_ap_tmp = pd.read_csv(r"http://lar.wsu.edu/R_apps/2020ap5/data/byAQSID/"+s+".apan")
        except:
            print('2020 data is not available')
        obs_ap=obs_ap_tmp
        
        #obs_ap=pd.read_csv(r"http://lar.wsu.edu/R_apps/2019ap5/data/byAQSID/530050003.apan")
        obs_ap['DateTime'] = pd.to_datetime(obs_ap['DateTime'])
        obs_ap.index = obs_ap['DateTime']
        delta = np.timedelta64(8,'h')
        obs_ap.index = obs_ap.index - delta
        
        obs_ap['O3_obs']=obs_ap['OZONEan'].copy()
        obs_ap['O3_ap']=obs_ap['OZONEap'].copy()
        #obs_ap.iloc[-24:,1]=np.nan
        #obs_ap=obs_ap.iloc[0:-24,].copy()
        #
        r = pd.date_range(start=obs_ap.index.min(), end=obs_ap.index.max(), freq='1H')
        obs_ap = obs_ap.reindex(r)
        
        obs_ap['O3avg8hr'] = obs_ap['O3_obs'].rolling(8, min_periods=6).mean()
        obs_ap['O3avg8hr_ap'] = obs_ap['O3_ap'].rolling(8, min_periods=6).mean()
        #obs_ap['O3avg8hr'] = obs_ap['O3avg8hr'].shift(-7)
        #fill na with ML forecasting
        #obs_ap['O3avg8hr']=obs_ap['O3avg8hr'].fillna(dict_his_tmp['mean'][dict_his_tmp['mean'].index.date<d]['O3_pred'])
        obs_ap['AQI_class'] = pd.cut(round(obs_ap['O3avg8hr'].fillna(-1)),
            [0, 54, 70, 85, 105, 200, np.inf],
            labels=[1, 2, 3, 4, 5, 6])
        obs_ap['AQI_class_ap'] = pd.cut(round(obs_ap['O3avg8hr_ap'].fillna(-1)),
            [0, 54, 70, 85, 105, 200, np.inf],
            labels=[1, 2, 3, 4, 5, 6])
        obs_ap['Past_8hr_O3'] = obs_ap['O3avg8hr'].shift(24)
        obs_ap['Past_hr_O3'] = obs_ap['O3_obs'].shift(24)
        obs_ap = obs_ap[['Past_hr_O3','O3_obs','O3_ap','Past_8hr_O3','AQI_class','O3avg8hr','AQI_class_ap','O3avg8hr_ap']]
        if len(obs_ap.dropna())==0: continue
        
        dict_his2 = pickle.load(f)
        df_tmp = dfPm.combine_first(dict_his2['WRFRT'])
        
        df_tmp = df_tmp.combine_first(obs_ap)
        df_tmp.update(obs_ap)
        
        #replace AIRNOW data with AQS data
        aqs_o3 = glob.glob('./data/AQS_data_US/44201/'+s+'_*_44201.csv')
        if len(aqs_o3) > 0:
            for o3_file in aqs_o3:
                aqs = pd.read_csv(o3_file)
                aqs = aqs[aqs['sample_duration']=='1 HOUR']
                aqs['sample_measurement'] = aqs['sample_measurement']*1000 #ppm to ppb
                if(len(aqs)==0): continue
                else: break
            aqs.index = pd.to_datetime(aqs.Datetime)
            #change time zone to local time
            delta = np.timedelta64(abs(gmtoff.loc[gmtoff['AQSID']==s,'GMToff'].values[0]),'h')
            aqs.index = aqs.index - delta
            aqs = aqs.drop_duplicates(subset='Datetime', keep="first") #There are duplicates at some sites. Based on the pre-gernerated data file, the first value should be used
            df_tmp = pd.concat([df_tmp,aqs],axis=1).drop(columns=['Datetime', 'O3_obs', 'units_of_measure', 'sample_duration'])
            df_tmp = df_tmp.rename(columns={"sample_measurement": "O3_obs"})
        else:
            print('NO AQS data')
            continue
        
        df_tmp['O3avg8hr'] = df_tmp['O3_obs'].rolling(8, min_periods=6).mean()
        #obs_ap['O3avg8hr'] = obs_ap['O3avg8hr'].shift(-7)
        #fill na with ML forecasting
        #obs_ap['O3avg8hr']=obs_ap['O3avg8hr'].fillna(dict_his_tmp['mean'][dict_his_tmp['mean'].index.date<d]['O3_pred'])
        df_tmp['AQI_class'] = pd.cut(round(df_tmp['O3avg8hr'].fillna(-1)),
            [0, 54, 70, 85, 105, 200, np.inf],
            labels=[1, 2, 3, 4, 5, 6])
        df_tmp['Past_hr_O3'] = df_tmp['O3_obs'].shift(24)
        
        df_tmp['Weekday']=df_tmp.index.dayofweek
        df_tmp['Month'] = df_tmp.index.month
        df_tmp['Hour'] = df_tmp.index.hour
        dfPm = df_tmp[['Past_hr_O3','O3_obs','O3_ap','O3avg8hr', 'O3avg8hr_ap', 'PBL_m', 'Surface_pres_Pa', 'Temp_K', 'U_m_s', 'V_m_s',
                            'RH_pct', 'Past_8hr_O3','Month','Hour','Weekday','AQI_class']].copy()
        dfPm = dfPm[(dfPm.index.month > 4 ) & (dfPm.index.month < 10)]
        
        dfPm.to_pickle(outputdir+"/data_wrf2020_"+s+".pkl")
        
    except:     
        print('2020 data is not available at '+s)
        