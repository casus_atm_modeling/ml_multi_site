import pylab
#pandas
import pandas as pd
print('pandas: %s' % pd.__version__)
#matplotlib
import matplotlib.pyplot as plt
#datetime
import datetime as dt
#import numpy
import numpy as np
#sklearn
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
#gaussian_kde
from scipy.stats import gaussian_kde
#anchored text
from matplotlib.offsetbox import AnchoredText 
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import RFE
from sklearn.model_selection import RepeatedKFold
import time
import glob
import pickle
import os

#open functions
exec(open(r"./ML_Functions.py").read())

gmtoff = pd.read_csv('./gmtoff.csv')

with open('./data/WRF_pkl/multi_sites_full_o3_1720.pkl', 'rb') as fp:
    dict_of_dt = pickle.load(fp)

#create the results dir if it doesn't exist
outputdir='./output/'
os.makedirs(outputdir, exist_ok=True)


for s in list(dict_of_dt.keys()):
    if os.path.isfile(outputdir+'data_o3_2RF_'+s+'.csv'): continue
    aqsid = s
    print(aqsid)
    
    dfPm = dict_of_dt[s]
    dfPm = dfPm[(dfPm.index.month >4) & (dfPm.index.month < 10)]
    #dfPm = dfPm.drop('O3_pred',1).dropna()
    if(len(dfPm)==0): continue
    
    try: 
        f = open("./data/WRF_pkl_repl_w_AQS/data_wrf2020_"+s+".pkl",'rb')
        dfPm = pickle.load(f)
    except:     
        print('2020 data is not available at '+s)

    print("site is ", s)
    print("dataframe columns are ", dfPm.head())
    
    delta = np.timedelta64(7,'h')
    dfPm.index = dfPm.index - delta
    dfPm = dfPm.dropna()
    X_org = dfPm.drop(['O3_obs', 'O3_ap', 'O3avg8hr', 'O3avg8hr_ap', 'Past_8hr_O3','AQI_class'], 1).copy()
    X_dat = pd.DataFrame(preprocess('MAS', X_org))
    X_dat.columns = X_org.keys()
    X_dat.index = X_org.index
    
    X = np.array(X_dat.copy())
    # separate "label" to y 
    y = np.array(dfPm['O3_obs'])
    
    #test
    #X = np.array(sorted(set(subdf.index.date)))
    #rkf = RepeatedKFold(n_splits=10, n_repeats=1, random_state=12883823)
    #for train_index,test_index in rkf.split(X):
    #    tmp = subdf[pd.to_datetime(subdf.index.date).isin(X[test_index])]
    #    print(tmp['Weekday'].value_counts().sort_index())
        
    all_date = np.array(sorted(set(dfPm.index.date)))
    rkf = RepeatedKFold(n_splits=10, n_repeats=10, random_state=12883823)
    a = 0
    dict_of_2019=dict()
    dict_of_max2019=dict()
    RF1_feature_table = pd.DataFrame(columns = dfPm.keys()[2:]) 
    RF2_feature_table = pd.DataFrame(columns = dfPm.keys()[2:])
    for train_index, test_index in rkf.split(all_date):
        train_datetime_index = pd.to_datetime(dfPm.index.date).isin(all_date[train_index])
        test_datetime_index = pd.to_datetime(dfPm.index.date).isin(all_date[test_index])
        X_train, X_test = X[train_datetime_index], X[test_datetime_index]
        y_train, y_test = y[train_datetime_index], y[test_datetime_index]
        
        # feature extraction
        model_RF = RandomForestRegressor(n_estimators=100, max_depth=7,random_state=137)
        model_RF = model_RF.fit(X_train, y_train)
        
        new_train = pd.DataFrame(X_train)
        new_train['obs_o3'] = y_train
        new_train['pred_o3'] = model_RF.predict(X_train)
        new_train['diff_o3'] = abs(new_train['pred_o3']-new_train['obs_o3'])
        new_train=new_train[new_train['diff_o3']>5]
        X_train2 = np.array(new_train.drop(['obs_o3','pred_o3','diff_o3'],axis=1))
        y_train2 = y_train[new_train.index]
        model_RF2 = RandomForestRegressor(n_estimators=200, max_depth=7,random_state=137)
        model_RF2 = model_RF2.fit(X_train2, y_train2)
        
        '''
        low_index = new_train[new_train['pred_o3']<40].index
        med_index = new_train[(new_train['pred_o3']<=60) & (new_train['pred_o3']>=40)].index
        high_index = new_train[new_train['pred_o3']>60].index
        '''
        pred1=model_RF.predict(X_train)
        pred2=model_RF2.predict(X_train)
        
        sep1 = np.percentile(pred1,33)
        sep2 = np.percentile(pred1,67)
        
        low_index = np.where(pred1<sep1)
        med_index = np.where((pred1<=sep2) & (pred1>=sep1))
        high_index = np.where(pred1>sep2)
        
        X_low=list (zip (pred1[low_index],pred2[low_index]))
        Y_low=y_train[low_index]
        model_LR = LinearRegression(fit_intercept=False)
        model_LR.fit(X_low, Y_low)
        low1=model_LR.coef_[0]
        low2=model_LR.coef_[1]
        
        X_med=list (zip (pred1[med_index],pred2[med_index]))
        Y_med=y_train[med_index]
        model_LR = LinearRegression(fit_intercept=False)
        model_LR.fit(X_med, Y_med)
        med1=model_LR.coef_[0]
        med2=model_LR.coef_[1]
        
        X_high=list (zip (pred1[high_index],pred2[high_index]))
        Y_high=y_train[high_index]
        model_LR = LinearRegression(fit_intercept=False)
        model_LR.fit(X_high, Y_high)
        high1=model_LR.coef_[0]
        high2=model_LR.coef_[1] 
        
        print('The coefficients are:')
        print(low1)
        print(low2)
        print(med1)
        print(med2)
        print(high1)
        print(high2)
        print('The length of three datasets are:')
        print(len(low_index[0]))
        print(len(med_index[0]))
        print(len(high_index[0]))
        print('The length of second training dataset are:')
        print(len(new_train)) 
                         
         
        #model_LR = LinearRegression()
        #rfe = RFE(model_LR, 5)
        #RFEfit = rfe.fit(X_train2, y_train2)
        #O3_pred = RFEfit.predict(X_test)
                           
        df2019 = X_test.copy() 
        pred1 = model_RF.predict(np.array(df2019))
        pred2 = model_RF2.predict(np.array(df2019))
        pred3=pred2
        pred3[np.where(pred1<sep1)] = low1*pred1[np.where(pred1<sep1)]+low2*pred2[np.where(pred1<sep1)]
        pred3[np.where((pred1>=sep1)&(pred1<=sep2))] = med1*pred1[np.where((pred1>=sep1)&(pred1<=sep2))]+med2*pred2[np.where((pred1>=sep1)&(pred1<=sep2))]
        pred3[np.where(pred1>sep2)] = high1*pred1[np.where(pred1>sep2)]+high2*pred2[np.where(pred1>sep2)]
        pred3[np.where(pred3<0)] = pred1[np.where(pred3<0)] 
        
        df2019 = dfPm[test_datetime_index].copy() 
        df2019 = df2019.assign(O3_pred=pred3)
        df2019.index = df2019.index + delta
        
        dict_name = str(a)
        dict_of_2019[dict_name] = df2019
        
        temp = df2019.copy()
        o32018 = temp[['O3_pred', 'O3_obs', 'O3_ap']].copy()
        r = pd.date_range(start=o32018.index.min(), end=o32018.index.max(), freq='1H')
        o32018 = o32018.reindex(r)
        o32018['O3_obs'] = o32018['O3_obs'].rolling(8, min_periods=6).mean()
        o32018['O3_ap'] = o32018['O3_ap'].rolling(8, min_periods=6).mean()
        o32018['O3_pred'] = o32018['O3_pred'].rolling(8, min_periods=6).mean()
        o32018['O3avg8hr_org'] = o32018['O3_obs'].shift(-7)
        o32018['O3avg8hr_RF'] = o32018['O3_pred'].shift(-7)
        o32018['O3avg8hr_ap'] = o32018['O3_ap'].shift(-7)
        o32018['O3_obs.maxdaily8hravg'] = o32018['O3avg8hr_org'].rolling(17, min_periods=13).max()
        o32018['O3_pred.maxdaily8hravg'] = o32018['O3avg8hr_RF'].rolling(17, min_periods=13).max() 
        o32018['O3_ap.maxdaily8hravg'] = o32018['O3avg8hr_ap'].rolling(17, min_periods=13).max()
        
        #shift columns
        o32018['O3_obs.maxdaily8hravg'] = o32018['O3_obs.maxdaily8hravg'].shift(-16)
        o32018['O3_pred.maxdaily8hravg'] = o32018['O3_pred.maxdaily8hravg'].shift(-16)
        o32018['O3_ap.maxdaily8hravg'] = o32018['O3_ap.maxdaily8hravg'].shift(-16)
        df2018dailyO38hrmax = o32018[(o32018.index.hour == 7)]#.dropna(how='all')
        
        df2018dailyO38hrmax['AQI_day'] = pd.cut(round(df2018dailyO38hrmax['O3_obs.maxdaily8hravg']),
                                [0, 54, 70, 85, 105, 200, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
        df2018dailyO38hrmax['AQI_pred_day'] = pd.cut(round(df2018dailyO38hrmax['O3_pred.maxdaily8hravg']),
                                [0, 54, 70, 85, 105, 200, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
        df2018dailyO38hrmax['AQI_ap_day'] = pd.cut(round(df2018dailyO38hrmax['O3_ap.maxdaily8hravg']),
                                [0, 54, 70, 85, 105, 200, np.inf],
                                labels=[1, 2, 3, 4, 5, 6])
    
        df2018dailyO38hrmax.index = df2018dailyO38hrmax.index+pd.DateOffset(hours=5)
        
        aqi_low=[0,51,101,151,201,301]
        aqi_high=[50,100,150,200,300,500]
        aqi_lowc=[0,55,71,86,106,201]
        aqi_highc=[54,70,85,105,200,600]
        df2018dailyO38hrmax['AQI']=np.nan
        for i in range(len(df2018dailyO38hrmax)):
            if(np.isnan(df2018dailyO38hrmax['O3_pred.maxdaily8hravg'][i])): continue
            #df2018dailyO38hrmax['AQI'][i] = aqi.to_iaqi(aqi.POLLUTANT_O3_8H, df2018dailyO38hrmax['O3_pred.maxdaily8hravg'][i]/1000, algo=aqi.ALGO_EPA)
            aqi_class=df2018dailyO38hrmax['AQI_pred_day'][i]-1
            df2018dailyO38hrmax['AQI'][i]=(aqi_high[aqi_class]-aqi_low[aqi_class])/(aqi_highc[aqi_class]-aqi_lowc[aqi_class])*(round(df2018dailyO38hrmax['O3_pred.maxdaily8hravg'][i])-aqi_lowc[aqi_class])+aqi_low[aqi_class]
            df2018dailyO38hrmax['AQI'][i]=round(df2018dailyO38hrmax['AQI'][i])
        
        dict_of_max2019[dict_name] = df2018dailyO38hrmax
        a=a+1
    
    for k in dict_of_max2019.keys():
        dict_of_max2019[k]['datetime']=dict_of_max2019[k].index
        
    big_df = pd.concat(dict_of_max2019)[['datetime','O3_obs.maxdaily8hravg','O3_ap.maxdaily8hravg','O3_pred.maxdaily8hravg']]
    big_df = big_df.groupby('datetime').mean().dropna(how='all')
    big_df['AQI_day'] = pd.cut(round(big_df['O3_obs.maxdaily8hravg']),
                            [0, 54, 70, 85, 105, 200, np.inf],
                            labels=[1, 2, 3, 4, 5, 6])
    big_df['AQI_pred_day'] = pd.cut(round(big_df['O3_pred.maxdaily8hravg']),
                            [0, 54, 70, 85, 105, 200, np.inf],
                            labels=[1, 2, 3, 4, 5, 6])
    big_df['AQI_ap_day'] = pd.cut(round(big_df['O3_ap.maxdaily8hravg']),
                            [0, 54, 70, 85, 105, 200, np.inf],
                            labels=[1, 2, 3, 4, 5, 6])
    big_df.to_csv(outputdir+'data_o3_2RF_'+s+'_8hrmax.csv')
