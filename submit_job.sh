#!/bin/bash
#SBATCH --job-name=O3_RFc
#SBATCH --time=15:00:00
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=O3_RFc.log 

#source ~/.bashrc
source  ~/miniconda3/etc/profile.d/conda.sh

conda activate MLkai

cd /home/lee45/play/DLair/ml_multi_site

python multi_o3_2RF_cv.py

